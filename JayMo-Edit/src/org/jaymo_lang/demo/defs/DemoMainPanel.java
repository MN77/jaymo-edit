/*******************************************************************************
 * Copyright (C): 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.demo.defs;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.function.Consumer;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import org.jaymo_lang.edit.button.GradientButton;
import org.jaymo_lang.edit.defs.TextOutput;
import org.jaymo_lang.edit.defs.TextResult;
import org.jaymo_lang.edit.editor.CodeEditor;

import de.mn77.base.data.group.Group4;
import de.mn77.base.event.Procedure;


/**
 * @author Michael Nitsche
 * @created 22.12.2019
 */
public class DemoMainPanel extends JPanel {

	private static final long serialVersionUID = 6516457542639499873L;

	public static final Font FONT             = new Font( Font.DIALOG_INPUT, Font.PLAIN, 15 );
	public static final int  FONT_CHAR_WIDTH  = 9;
	public static final int  FONT_LINE_HEIGHT = 18;

	private final Consumer<Group4<String, JTextArea, JTextField, Procedure>> proc;
	private final Procedure                                                  reset;
//	final JTextArea compCode = new JTextArea();
	private final CodeEditor compCode    = new CodeEditor( DemoMainPanel.FONT_CHAR_WIDTH, DemoMainPanel.FONT_LINE_HEIGHT );
	private final TextOutput compOutput  = new TextOutput();
	private final TextResult compResult  = new TextResult();
	private final JButton    buttonExec  = new JButton( "Execute" );
	private final JButton    buttonClear = new GradientButton( "Clear", new Color( 127, 255, 127 ) );
	private final JButton    buttonReset = new GradientButton( "Reset", new Color( 255, 143, 143 ) );


	public DemoMainPanel( final Consumer<Group4<String, JTextArea, JTextField, Procedure>> proc, final Procedure reset ) {
		super( new FlowLayout() );

		this.proc = proc;
		this.reset = reset;

		this.compCode.setFont( DemoMainPanel.FONT ); // Default: family=Dialog,name=Dialog,style=plain,size=12		// family=Monospaced,name=Monospaced,style=plain,size=14
		this.compOutput.setFont( DemoMainPanel.FONT );
		this.compResult.setFont( DemoMainPanel.FONT );

		this.buttonReset.setBackground( new Color( 255, 143, 143 ) );
		this.buttonExec.setToolTipText( "Ctrl + Enter" );

		final JScrollPane scroll1 = new JScrollPane( this.compCode );
		final JScrollPane scroll2 = new JScrollPane( this.compOutput );

		scroll1.getViewport().setBackground( Color.white ); // Background for the editor
		scroll1.setOpaque( true );

		this.initEvents();

		// --- LEFT ---

		final JPanel left = new JPanel();
		final SpringLayout leftLayout = new SpringLayout();
		left.setLayout( leftLayout );
		left.add( scroll1 );
		left.add( this.buttonExec );
		left.add( this.buttonClear );
		left.add( this.buttonReset );

		leftLayout.putConstraint( SpringLayout.EAST, this.buttonReset, -5, SpringLayout.EAST, left );
		leftLayout.putConstraint( SpringLayout.SOUTH, this.buttonReset, -5, SpringLayout.SOUTH, left );
		this.buttonReset.setPreferredSize( new Dimension( 75, 30 ) );

		leftLayout.putConstraint( SpringLayout.EAST, this.buttonClear, -5, SpringLayout.WEST, this.buttonReset );
		leftLayout.putConstraint( SpringLayout.SOUTH, this.buttonClear, -5, SpringLayout.SOUTH, left );
		this.buttonClear.setPreferredSize( new Dimension( 75, 30 ) );

		leftLayout.putConstraint( SpringLayout.WEST, this.buttonExec, 5, SpringLayout.WEST, left );
		leftLayout.putConstraint( SpringLayout.EAST, this.buttonExec, -5, SpringLayout.WEST, this.buttonClear );
		leftLayout.putConstraint( SpringLayout.SOUTH, this.buttonExec, -5, SpringLayout.SOUTH, left );
		this.buttonExec.setPreferredSize( new Dimension( 200, 30 ) );

		leftLayout.putConstraint( SpringLayout.WEST, scroll1, 5, SpringLayout.WEST, left );
		leftLayout.putConstraint( SpringLayout.NORTH, scroll1, 5, SpringLayout.NORTH, left );
		leftLayout.putConstraint( SpringLayout.EAST, scroll1, -5, SpringLayout.EAST, left );
		leftLayout.putConstraint( SpringLayout.SOUTH, scroll1, -5, SpringLayout.NORTH, this.buttonExec );


		// --- RIGHT ---

		final JPanel right = new JPanel();
		final SpringLayout rightLayout = new SpringLayout();
		right.setLayout( rightLayout );
		right.add( scroll2 );
		right.add( this.compResult );

		rightLayout.putConstraint( SpringLayout.WEST, this.compResult, 5, SpringLayout.WEST, right );
		rightLayout.putConstraint( SpringLayout.EAST, this.compResult, -5, SpringLayout.EAST, right );
		rightLayout.putConstraint( SpringLayout.SOUTH, this.compResult, -5, SpringLayout.SOUTH, right );

		rightLayout.putConstraint( SpringLayout.WEST, scroll2, 5, SpringLayout.WEST, right );
		rightLayout.putConstraint( SpringLayout.NORTH, scroll2, 5, SpringLayout.NORTH, right );
		rightLayout.putConstraint( SpringLayout.EAST, scroll2, -5, SpringLayout.EAST, right );
		rightLayout.putConstraint( SpringLayout.SOUTH, scroll2, -5, SpringLayout.NORTH, this.compResult );


		// --- MAIN ---

		final Container panel3 = this;

		final GridLayout layout2 = new GridLayout();
		layout2.setColumns( 2 );
		layout2.setRows( 1 );
		panel3.setLayout( layout2 );

		panel3.add( left );
		panel3.add( right );
	}

	public void execute() {
		if( !this.buttonExec.isEnabled() )
			return;

		final String source = DemoMainPanel.this.compCode.getText();
		this.buttonExec.setEnabled( false );
		final Procedure onFinish = () -> DemoMainPanel.this.buttonExec.setEnabled( true );

		this.proc.accept( new Group4<>( source, DemoMainPanel.this.compOutput, DemoMainPanel.this.compResult, onFinish ) );
	}

	private void initEvents() {
//		this.compCode.addKeyListener(new KeyListener() {
//		this.addKeyListener(new KeyListener() {
//			public void keyTyped(final KeyEvent e) {}
//			public void keyPressed(final KeyEvent e) {}
//			public void keyReleased(final KeyEvent e) {
////				MOut.temp(e.getExtendedKeyCode(), e.getID(), e.getKeyChar(), e.getKeyCode(), e.getKeyLocation(), e.getModifiers(), e.getModifiersEx(), e.getSource());
//				if(e.getKeyCode() == 10 && e.getModifiers() > 0)
//					MainPanel.this.execute();
//			}
//		});

		this.buttonExec.addActionListener( event -> {
			this.compOutput.setText( "" );
			this.compResult.setText( "" );
			this.execute();
		} );

		this.buttonReset.addActionListener( event -> {
			this.reset.execute();
			this.buttonExec.setEnabled( true );
		} );

		this.buttonClear.addActionListener( event -> {
			this.compCode.setText( "" );
			this.compOutput.setText( "" );
			this.compResult.setText( "" );
		} );
	}

}
