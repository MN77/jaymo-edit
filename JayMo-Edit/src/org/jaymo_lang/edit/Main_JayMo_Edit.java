/*******************************************************************************
 * Copyright (C): 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit;

import java.awt.Frame;
import java.awt.KeyboardFocusManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.WindowConstants;

import org.jaymo_lang.JayMo;
import org.jaymo_lang.edit.action.EVENTS;
import org.jaymo_lang.edit.action.JayMo_Actions;
import org.jaymo_lang.edit.defs.FileEditor;
import org.jaymo_lang.edit.defs.ToolbarPanel;
import org.jaymo_lang.object.atom.JMo_Bool;
import org.jaymo_lang.object.atom.JMo_Dec;
import org.jaymo_lang.object.atom.JMo_Double;
import org.jaymo_lang.object.atom.JMo_Int;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.sys.JMo_Input;
import org.jaymo_lang.parser.Parser_App;

import de.mn77.base.data.util.Lib_Random;
import de.mn77.base.data.util.U_StringArray;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.error.Err_Runtime;
import de.mn77.base.sys.MOut;
import de.mn77.base.version.Lib_Version;
import de.mn77.base.version.data.VersionData_ABC;
import de.mn77.lib.swing.DIALOG_TYPE;
import de.mn77.lib.swing.Lib_SwingDialog;


/**
 * @author Michael Nitsche
 * @created 16.03.2021
 */
public class Main_JayMo_Edit {

	public static final VersionData_ABC VERSION       = new VersionData_ABC( 0, 6, 2 ); // Major, Minor, Fix
	public static final int             DEFAULT_WIDTH = 970, DEFAULT_HEIGHT = 600; // 16:9,888     // 16:10 = 960:600
	public static final String          NAME          = "JayMo-Edit";

	public static final String FONT_FILE        = "/jar/font/liberation2/LiberationMono-Regular.ttf";
	public static final float  FONT_SIZE        = 15f;
	public static final int    FONT_WIDTH       = 9;	// Tab width
	public static final int    FONT_HEIGHT      = 18; // First line background
	public static final String BORDER_FONT_FILE = "/jar/font/dejavu/DejaVuSansMono.ttf";


	private final JFrame frame;


	private final ToolbarPanel  panel;
	private final JayMo_Actions actions;


	public static void main( final String[] args ) {
		final String[] args2 = Main_JayMo_Edit.args( args );
		MOut.setJavaErrors( false );

		try {
			Lib_Version.init( Main_JayMo_Edit.VERSION, false );
		}
		catch( final Err_FileSys e ) {
			Err.show( e );
		}

//		System.setProperty("awt.useSystemAAFontSettings","true");	// okay
//		System.setProperty("awt.useSystemAAFontSettings","on");		// okay
//		System.setProperty("awt.useSystemAAFontSettings","false");	// really bad
		System.setProperty( "awt.useSystemAAFontSettings", "lcd" ); // very clear	// best
//		System.setProperty("awt.useSystemAAFontSettings","gasp");	// okay ... this is maybe the default

		if( args2 != null ) { // otherwise don't start and exit directly!
			final File initFile = args2.length == 1 ? new File( args2[0] ) : null;
			javax.swing.SwingUtilities.invokeLater( () -> new Main_JayMo_Edit( initFile ).createAndShowGUI() );
		}
	}

	@SuppressWarnings( "unused" )
	private static String[] args( final String[] args ) {

		for( int i = 0; i < args.length; i++ ) {
			final String s = args[i].trim();

			if( !s.startsWith( "-" ) ) {
				final String[] args2 = U_StringArray.cutFrom( args, i );

				// Allow only one argument
				if( args2.length > 1 )
					throw new Err_Runtime( "Invalid amount of arguments" );

				return args2;
			}
			else
				switch( s ) {
//					case "-d":
//					case "--debug":
//						this.debug = true;
//						MOut.setJavaErrors(true);
//						MOut.debugDetail();
//						break;
					case "--version":
						final StringBuilder sb = new StringBuilder();
						sb.append( Main_JayMo_Edit.NAME );
						sb.append( " " );
						sb.append( Main_JayMo_Edit.VERSION.toStringShort() );
						sb.append( "  /  " );
						sb.append( JayMo.NAME );
						sb.append( " " );
						sb.append( new Parser_App().getVersionString( JayMo.STAGE.isDevelopment(), false, true ) );
						MOut.print( sb.toString() );
						return null;
//					case "--help":
//						MOut.print(Lib_CliInfo.help());
//						return null;

					default:
						throw new Err_Runtime( "Unknown argument for " + Main_JayMo_Edit.NAME, s );
				}
		}
		return new String[0];
	}

	public Main_JayMo_Edit( final File initFile ) {
		this.styleUI();
		this.frame = new JFrame();
		this.actions = new JayMo_Actions( this, this.frame );
		this.panel = new ToolbarPanel( this.actions );

		this.initInputSource();
		this.updateTitle( null );

		if( initFile != null )
			this.actions.open( initFile );
	}

	public void dispose() {
		this.frame.dispose();
	}

	public void setStatus( final String s ) {
		this.panel.setStatus( s );
	}

	public void updateTitle( final FileEditor editor ) {
//		String title = "JayMo Light  " + this.parser.getVersionString(false, false);
//		String title = "JayMo Light  " + JayMo_Swing.VERSION.toString_Short();		// .toFormat("%1.%2.%3%D")
		String title = Main_JayMo_Edit.NAME;

		if( editor != null ) {
			if( editor.getCurrentFile() != null )
				title += " - " + editor.getCurrentFile().getAbsolutePath();
			if( editor.hasUnsavedChanges() )
				title += " *";
		}
		this.frame.setTitle( title );
	}

	private void addKeyEvents( final JayMo_Actions actions ) {
		KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher( e -> {
			if( KeyEvent.KEY_RELEASED != e.getID() )
				return false;

			final int key = e.getKeyCode();
			final int mod = e.getModifiersEx();
			final int CTRL = Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx(); // InputEvent.CTRL_DOWN_MASK
			final int ALT = InputEvent.ALT_DOWN_MASK;
			final int SHIFT = InputEvent.SHIFT_DOWN_MASK;

			//TODO Tasten werden auch gefangen, wenn das Fenster erst nach dem Schließen eines anderen (z.B. Terminal mit Strg+D) den Fokus erhält.

			if( key == 81 && mod == CTRL ) // Strg + Q
				actions.perform( EVENTS.EXIT );
			else if( key == 79 && mod == CTRL ) // Strg + O
				actions.perform( EVENTS.OPEN );
			else if( key == 83 && mod == CTRL ) // Strg + S
				actions.perform( EVENTS.SAVE );
			else if( key == 83 && mod == CTRL + InputEvent.SHIFT_DOWN_MASK ) // Strg + Shift + S
				actions.perform( EVENTS.SAVE_AS );

//			else if(key == 90 && mod == 2) // Strg + Z
//				panel.getComponentMain().getComponentEditor().getHistory().undo();
//			else if(key == 90 && mod == 3) // Strg + Shift + Z
//				panel.getComponentMain().getComponentEditor().getHistory().redo();

			else if( (key == 10 || key == 153) && mod == CTRL ) // Strg + Enter
				actions.perform( EVENTS.EXEC );
			else if( key == 112 && mod == 0 ) // F1
				actions.perform( EVENTS.INFO );
			else if( key == 113 && mod == 0 ) // F2
				actions.perform( EVENTS.SAVE );
			else if( key == 115 && mod == 0 ) // F4
				actions.perform( EVENTS.SLIDER );
			else if( key == 120 && mod == 0 ) // F9
				actions.perform( EVENTS.EXEC );
			else if( key == 121 && mod == 0 ) // F10
				actions.perform( EVENTS.STOP );

			else if( key == 122 && mod == 0 || key == 10 && mod == ALT ) { // F11, ALT + Enter
				final boolean isMax = this.frame.getExtendedState() == Frame.MAXIMIZED_BOTH;
				this.frame.setExtendedState( isMax ? Frame.NORMAL : Frame.MAXIMIZED_BOTH );
//				this.frame.setUndecorated(true);
			}
			else if( key == 123 && mod == 0 ) // F12
				actions.perform( EVENTS.WEB );

			else if( key == 'F' && mod == CTRL )
				actions.perform( EVENTS.FIND );

//			else if(key == 'G' && mod == ctrl)
//				Lib_SwingDialog.okay(this.frame, DIALOG_TYPE.INFO, "Find next", "Find-Feature is comming soon.");
			else if( e.getKeyCode() == 'H' && mod == CTRL )
				Lib_SwingDialog.okay( this.frame, DIALOG_TYPE.INFO, "Find & Replace", "Replace-Feature is comming soon." );

			else if( key == 'A' && mod == CTRL + SHIFT )
				actions.perform( EVENTS.SELECT_NONE );

			else
				return false;

//			MOut.temp(key, mod);
			return true; // No further action to that key, it was matched here
		} );
	}

	private void createAndShowGUI() {
		this.frame.setDefaultCloseOperation( WindowConstants.EXIT_ON_CLOSE );

		this.frame.add( this.panel );


		this.frame.pack();
		this.frame.setSize( Main_JayMo_Edit.DEFAULT_WIDTH, Main_JayMo_Edit.DEFAULT_HEIGHT );
		this.frame.setVisible( true );

		final ImageIcon img = new ImageIcon( this.getClass().getClassLoader().getResource( "jar/logo/jaymo_icon_256.png" ) );
		this.frame.setIconImage( img.getImage() );

		this.frame.addWindowListener( new WindowListener() {

			public void windowActivated( final WindowEvent e ) {}

			public void windowClosed( final WindowEvent e ) {}

			public void windowClosing( final WindowEvent e ) {
				Main_JayMo_Edit.this.actions.actionPerformed( new ActionEvent( this, Lib_Random.getInt(), EVENTS.EXIT.name() ) );
			}

			public void windowDeactivated( final WindowEvent e ) {}

			public void windowDeiconified( final WindowEvent e ) {}

			public void windowIconified( final WindowEvent e ) {}

			public void windowOpened( final WindowEvent e ) {}

		} );

		this.addKeyEvents( this.actions );
		this.panel.setFocus();
	}

	private void initInputSource() {
		JMo_Input.setSource( ( final Class<?> type ) -> {
			String message = "Please input";

			if( type == JMo_Str.class )
				message += " a string";
			else if( type == JMo_Int.class )
				message += " a integer";
			else if( type == JMo_Dec.class || type == JMo_Double.class )
				message += " a decimal number";
			else if( type == JMo_Bool.class )
				message += " true or false";

			return Lib_SwingDialog.input( this.frame, "Input", message, null );
		} );
	}

	private void styleUI() {
//		UIManager.put("swing.boldMetal", Boolean.FALSE);
//		Main_IDE_Swing.setUIFont (new javax.swing.plaf.FontUIResource("DejaVu Sans",Font.ITALIC,12));

//		try {
//		    for(LookAndFeelInfo info : UIManager.getInstalledLookAndFeels())
//		        MOut.print(info.getName(), info.getClassName());

//			UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
//			UIManager.setLookAndFeel( "com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
//			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());

//			UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
//			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
//			UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
//			UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");

//			SwingUtilities.updateComponentTreeUI(frame);
//		}
//		catch(ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e1) {
//			Err.show(e1);
//		}
	}

//	private static void setUIFont(final javax.swing.plaf.FontUIResource fr) {
//		final java.util.Enumeration keys = UIManager.getDefaults().keys();
//		while(keys.hasMoreElements()) {
//			final Object key = keys.nextElement();
//			final Object value = UIManager.get(key);
//			if(value instanceof javax.swing.plaf.FontUIResource)
//				UIManager.put(key, fr);
//		}
//	}

}
