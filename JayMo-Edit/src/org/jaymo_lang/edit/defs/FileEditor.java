/*******************************************************************************
 * Copyright (C): 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit.defs;

import java.io.File;

import org.jaymo_lang.edit.action.EVENTS;
import org.jaymo_lang.edit.action.JayMo_Actions;
import org.jaymo_lang.edit.editor.CodeEditor;
import org.jaymo_lang.edit.editor.styler.Styler;
import org.jaymo_lang.edit.lib.RecentFiles;


/**
 * @author Michael Nitsche
 * @created 31.03.2021
 */
public class FileEditor extends CodeEditor {

	private static final long serialVersionUID = 7841170632314601549L;

	private File                currentFile = null;
	private boolean             isChanged   = false;
	private final JayMo_Actions actions;
	private final RecentFiles   recent;


	public FileEditor( final int font_char_width, final int font_line_hight, final JayMo_Actions actions ) {
		super( font_char_width, font_line_hight );
		this.setStyler( new Styler( this ) );
		this.actions = actions;
		this.recent = new RecentFiles();
		this.recent.init( actions );
	}

	public File getCurrentFile() {
		return this.currentFile;
	}

	public RecentFiles getRecent() {
		return this.recent;
	}

	public boolean hasUnsavedChanges() {
		return this.isChanged && (this.currentFile != null || this.getText().trim().length() > 0);
	}

	public void setChanged( final boolean b ) {
		final boolean updateNeeded = b != this.isChanged;
		this.isChanged = b;
		if( updateNeeded )
			this.actions.perform( EVENTS.CHANGED );
	}

	public void setCurrentFile( final File f ) {
		this.currentFile = f;
		this.actions.perform( EVENTS.CHANGED );
	}

}
