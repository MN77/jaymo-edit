/*******************************************************************************
 * Copyright (C): 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit.defs;

import java.awt.Insets;

import javax.swing.JTextArea;


/**
 * @author Michael Nitsche
 * @created 28.10.2020
 */
public class TextOutput extends JTextArea {

	private static final long serialVersionUID = -6678000105926112037L;


	public TextOutput() {
		super();

		this.setEditable( false );
		this.setLineWrap( false );
		this.setMargin( new Insets( 5, 5, 5, 5 ) );
		this.setMargin( new Insets( 0, 8, 0, 8 ) ); // No margin at top and bottom // Sync with TestResult
		this.setTabSize( 4 );
//		this.setBackground(new Color(238,238,238));
		this.setOpaque( true ); // non-transparent

//		Border border = BorderUIResource.getEtchedBorderUIResource();
//		this.setBorder(border);
//		this.setBorder(null);		//#B8CFE5
	}

}
