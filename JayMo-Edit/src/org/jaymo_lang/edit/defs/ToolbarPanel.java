/*******************************************************************************
 * Copyright (C): 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit.defs;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToolBar;

import org.jaymo_lang.JayMo;
import org.jaymo_lang.edit.action.EVENTS;
import org.jaymo_lang.edit.action.JayMo_Actions;
import org.jaymo_lang.edit.editor.context.EditorContextMenu;

import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 16.03.2021
 */
public class ToolbarPanel extends JPanel {

	private static final long serialVersionUID = 6516457542639499813L;

	private final EditorMainPanel main;
	private final StatusBar       statusBar;


	public ToolbarPanel( final JayMo_Actions actions ) {
		super( new BorderLayout() );
		this.main = new EditorMainPanel( actions );
		this.statusBar = new StatusBar();

		actions.setMain( this.main );
		EditorContextMenu.addTo( this.main.getComponentEditor(), actions );

		final JToolBar toolBar = new JToolBar( "Toolbar 1" );
		toolBar.setFloatable( false ); // Not dragable
		toolBar.setRollover( true ); // Hover on mouse over

		this.addButtons( actions, toolBar );

		this.setPreferredSize( new Dimension( 450, 130 ) );
		this.add( toolBar, BorderLayout.PAGE_START );
		this.add( this.main, BorderLayout.CENTER );
		this.add( this.statusBar, BorderLayout.PAGE_END );
	}

	public void setFocus() {
		this.main.setFocus();
	}

	public void setStatus( final String s ) {
		this.statusBar.setMessage( s );
	}

	protected JButton iCreateButton( final JayMo_Actions actions, final String imagePath, final String altText, final EVENTS ev, String toolTipText, final String hotkey ) {
		final URL imageURL = ToolbarPanel.class.getResource( imagePath );

		final JButton button = new JButton();
		button.setActionCommand( ev.name() );
		if( hotkey != null )
			toolTipText += "  (" + hotkey + ')';
		button.setToolTipText( toolTipText );
		button.addActionListener( actions );

		if( imageURL != null )
			button.setIcon( new ImageIcon( imageURL, altText ) );
		else
			//no image found
//			button.setText(altText);
			MOut.error( "Image not found: " + imagePath );

		return button;
	}

	private void addButtons( final JayMo_Actions actions, final JToolBar toolBar ) {
		final JLabel image = new JLabel();
		final ImageIcon img = new ImageIcon( this.getClass().getClassLoader().getResource( "jar/logo/jaymo_toolbar_76x36.png" ) );
		image.setIcon( img );
		toolBar.add( image );

		toolBar.addSeparator(); //-----------------------------------------------

		JButton button = null;

		button = this.iCreateButton( actions,
			"/jar/icon/document-new.png", "New",
			EVENTS.NEW,
			"New script", null );
		toolBar.add( button );

		button = this.iCreateButton( actions,
			"/jar/icon/document-open.png", "Open",
			EVENTS.OPEN,
			"Load a script", "STRG + o" );
		toolBar.add( button );

		button = this.iCreateButton( actions,
			"/jar/icon/x-office-address-book.png", "Recent", //bookmark-new.png	image-loading.png	office-calendar.png		system-file-manager.png
			EVENTS.RECENT,
			"Open a recent edited script", null );
		toolBar.add( button );

		button = this.iCreateButton( actions,
			"/jar/icon/document-save.png", "Save",
			EVENTS.SAVE,
			"Save script", "STRG + s" );
		toolBar.add( button );

		button = this.iCreateButton( actions,
			"/jar/icon/document-save-as.png", "Save as",
			EVENTS.SAVE_AS,
			"Save script as", "STRG + SHIFT + s" );
		toolBar.add( button );

		toolBar.addSeparator(); //-----------------------------------------------

		final JButton undoButton = this.iCreateButton( actions,
			"/jar/icon/edit-undo.png", "Undo",
			EVENTS.UNDO,
			"Undo", "STRG + z" );
		toolBar.add( undoButton );

		final JButton redoButton = this.iCreateButton( actions,
			"/jar/icon/edit-redo.png", "Redo",
			EVENTS.REDO,
			"Redo", "STRG + y" ); // STRG + SHIFT + z
		toolBar.add( redoButton );

		this.main.getComponentEditor().getHistory().eChanged( c -> {
			undoButton.setEnabled( c.canUndo() );
			redoButton.setEnabled( c.canRedo() );
		} );
		undoButton.setEnabled( false );
		redoButton.setEnabled( false );

		toolBar.addSeparator(); //-----------------------------------------------

//		button = this.iCreateButton(actions,
//			"/jar/icon/edit-clear.png", "Clear",
//			EVENTS.CLEAR,
//			"Clear the editor", null);
//		toolBar.add(button);
//
//		toolBar.addSeparator(); //-----------------------------------------------

		button = this.iCreateButton( actions,
			"/jar/icon/edit-cut.png", "Cut",
			EVENTS.CUT,
			"Cut", "STRG + x" );
		toolBar.add( button );

		button = this.iCreateButton( actions,
			"/jar/icon/edit-copy.png", "Copy",
			EVENTS.COPY,
			"Copy", "STRG + c" );
		toolBar.add( button );

		button = this.iCreateButton( actions,
			"/jar/icon/edit-paste.png", "Paste",
			EVENTS.PASTE,
			"Paste", "STRG + v" );
		toolBar.add( button );

		toolBar.addSeparator(); //-----------------------------------------------

		button = this.iCreateButton( actions,
			"/jar/icon/format-indent-less.png", "Indent-",
			EVENTS.OUTDENT,
			"Less indent", "SHIFT + Tab" );
		toolBar.add( button );

		button = this.iCreateButton( actions,
			"/jar/icon/format-indent-more.png", "Indent+",
			EVENTS.INDENT,
			"More indent", "Tab" );
		toolBar.add( button );

		button = this.iCreateButton( actions,
			"/jar/icon/edit-find.png", "Find",
			EVENTS.FIND,
			"Find", "STRG + f" );
		toolBar.add( button );

		toolBar.addSeparator(); //-----------------------------------------------

		button = this.iCreateButton( actions,
			"/jar/icon/media/media-playback-start_green.png", "Execute", //go-next	start-here
			EVENTS.EXEC,
			"Execute script", "F9" ); //STRG + Enter	// STRG + <
		toolBar.add( button );
		final JButton execButton = button;

		button = this.iCreateButton( actions,
			"/jar/icon/media/media-playback-stop_red.png", "Stop", //process-stop
			EVENTS.STOP,
			"Stop execution", "F10" );
		toolBar.add( button );
		button.setEnabled( false );
		final JButton terminateButton = button;

		toolBar.addSeparator(); //-----------------------------------------------

		button = this.iCreateButton( actions,
			"/jar/icon/help-browser.png", "Info",
			EVENTS.INFO,
			"Informations about JayMo", "F1" );
		toolBar.add( button );

		button = this.iCreateButton( actions,
			"/jar/icon/applications-internet.png", "Website", // internet-web-browser.png
			EVENTS.WEB,
			"Go to '" + JayMo.WEB_HOME_SHORT + "'", "F12" );
		toolBar.add( button );

		actions.setButtons( execButton, terminateButton );
	}

}
