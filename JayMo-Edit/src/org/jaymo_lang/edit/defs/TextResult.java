/*******************************************************************************
 * Copyright (C): 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit.defs;

import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.plaf.BorderUIResource;


/**
 * @author Michael Nitsche
 * @created 28.10.2020
 */
public class TextResult extends JTextField {

	private static final long serialVersionUID = -7236082049980028092L;


	public TextResult() {
		super();

		this.setEditable( false );
//		this.setMargin(new Insets(5, 5, 5, 5));	// Tut nicht
//		Border border = new LineBorder(new Color(192,192,192), 0);
//		Border border = new LineBorder(new Color(122,138,153), 1);
		final Border border = BorderUIResource.getEtchedBorderUIResource();
		this.setBorder( border );
	}

	public void showMessage( final String message ) {
		this.setText( " " + message );
	}

}
