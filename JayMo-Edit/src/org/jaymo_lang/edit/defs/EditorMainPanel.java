/*******************************************************************************
 * Copyright (C): 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit.defs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.io.InputStream;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.SpringLayout;
import javax.swing.plaf.BorderUIResource;

import org.jaymo_lang.edit.Main_JayMo_Edit;
import org.jaymo_lang.edit.action.JayMo_Actions;

import de.mn77.base.sys.file.Lib_Jar;


/**
 * @author Michael Nitsche
 * @created 16.03.2021
 */
public class EditorMainPanel extends JPanel {

	private static final long serialVersionUID = 6516457542639499873L;

	private final FileEditor compCode;
	private final TextOutput compOutput = new TextOutput();
	private final TextResult compResult = new TextResult();
	private JSplitPane       splitPane  = null;


	public EditorMainPanel( final JayMo_Actions actions ) {
		super( new FlowLayout() );

		// --- FONT ---

		Font font = null;
		int font_char_width = -1;
		int font_line_height = -1;

		try {
//			final InputStream is = new MFile("/usr/share/fonts/truetype/dejavu/DejaVuSansMono.ttf").read();
//			final InputStream is = Lib_Jar.getJarStream("/jar/font/dejavu/DejaVuSansMono.ttf");
//			font = Font.createFont(Font.TRUETYPE_FONT, is).deriveFont(Font.PLAIN, 14); // DejaVu 13 = Monospace 10
//			font_char_width = 8;
//			font_line_height = 17;

			final InputStream is = Lib_Jar.getStream( Main_JayMo_Edit.FONT_FILE );
			font = Font.createFont( Font.TRUETYPE_FONT, is ).deriveFont( Main_JayMo_Edit.FONT_SIZE );
			font_char_width = Main_JayMo_Edit.FONT_WIDTH;
			font_line_height = Main_JayMo_Edit.FONT_HEIGHT;

//			final InputStream is = Lib_Jar.getJarStream("/jar/font/dejavu/DejaVuSansMono-Bold.ttf");
//			font = Font.createFont(Font.TRUETYPE_FONT, is).deriveFont(15f);
//			font_char_width = 9;
//			font_line_height = 18;

//			font = new Font(Font.DIALOG_INPUT, Font.PLAIN, 14);
//			font_char_width = 8;
//			font_line_height = 17;

//			font = new Font(Font.DIALOG_INPUT, Font.PLAIN, 15);
//			font_char_width = 9;
//			font_line_height = 18;

//			final InputStream is = Lib_Jar.getJarStream("/jar/font/liberation2/LiberationMono-Regular.ttf");
//			font = Font.createFont(Font.TRUETYPE_FONT, is).deriveFont(14f);
//			font_char_width = 8;
//			font_line_height = 17;

//			final InputStream is = Lib_Jar.getJarStream("/jar/font/liberation2/LiberationMono-Regular.ttf");
//			font = Font.createFont(Font.TRUETYPE_FONT, is).deriveFont(15f);
//			font_char_width = 9;
//			font_line_height = 18;
		}
		catch( final Exception e ) {
			// Fallback
			font = new Font( Font.DIALOG_INPUT, Font.PLAIN, 14 );
//			font = new Font(Font.MONOSPACED, Font.PLAIN, 14);
			font_char_width = 8;
			font_line_height = 17;
		}

		this.compCode = new FileEditor( font_char_width, font_line_height, actions );

		this.compCode.setFont( font );
		this.compOutput.setFont( font );
		this.compResult.setFont( font );

		final JScrollPane scroll1 = new JScrollPane( this.compCode );
		final JScrollPane scroll2 = new JScrollPane( this.compOutput );

		scroll1.getViewport().setBackground( Color.white ); // Background for the editor
		scroll1.setOpaque( true );
		scroll1.setBorder( BorderUIResource.getEtchedBorderUIResource() );
		scroll2.setBorder( BorderUIResource.getEtchedBorderUIResource() );


		// --- LEFT ---

		final JPanel left = new JPanel();
		final SpringLayout leftLayout = new SpringLayout();
		left.setLayout( leftLayout );
		left.add( scroll1 );

		leftLayout.putConstraint( SpringLayout.WEST, scroll1, 0, SpringLayout.WEST, left );
		leftLayout.putConstraint( SpringLayout.NORTH, scroll1, 0, SpringLayout.NORTH, left );
		leftLayout.putConstraint( SpringLayout.EAST, scroll1, 0, SpringLayout.EAST, left );
		leftLayout.putConstraint( SpringLayout.SOUTH, scroll1, 0, SpringLayout.SOUTH, left );


		// --- RIGHT ---

		final JPanel right = new JPanel();
		final SpringLayout rightLayout = new SpringLayout();
		right.setLayout( rightLayout );
		right.add( scroll2 );
		right.add( this.compResult );

		rightLayout.putConstraint( SpringLayout.WEST, this.compResult, 0, SpringLayout.WEST, right );
		rightLayout.putConstraint( SpringLayout.EAST, this.compResult, 0, SpringLayout.EAST, right );
		rightLayout.putConstraint( SpringLayout.SOUTH, this.compResult, 0, SpringLayout.SOUTH, right );

		rightLayout.putConstraint( SpringLayout.WEST, scroll2, 0, SpringLayout.WEST, right );
		rightLayout.putConstraint( SpringLayout.NORTH, scroll2, 0, SpringLayout.NORTH, right );
		rightLayout.putConstraint( SpringLayout.EAST, scroll2, 0, SpringLayout.EAST, right );
		rightLayout.putConstraint( SpringLayout.SOUTH, scroll2, 0, SpringLayout.NORTH, this.compResult );


		// --- MAIN ---

		this.splitPane = new JSplitPane( JSplitPane.HORIZONTAL_SPLIT, true, left, right );

		left.setMinimumSize( new Dimension( 100, 100 ) );
		right.setMinimumSize( new Dimension( 0, 0 ) );
		this.splitPane.setResizeWeight( 0.5 ); // 1
		this.splitPane.setDividerLocation( Integer.MAX_VALUE );

		final GridLayout layout2 = new GridLayout();
		layout2.setColumns( 1 );
		layout2.setRows( 1 );
		this.setLayout( layout2 );
		this.add( this.splitPane );
	}

	public void clearOutput() {
		this.compOutput.setText( "" );
		this.compResult.setText( "" );
	}

	public String getCode() {
		return this.compCode.getText();
	}

	public FileEditor getComponentEditor() {
		return this.compCode;
	}

	public TextOutput getComponentOutput() {
		return this.compOutput;
	}

	public TextResult getComponentResult() {
		return this.compResult;
	}

	public JSplitPane getSplit() {
		return this.splitPane;
	}

	public void setFocus() {
		this.compCode.grabFocus();
	}

	public void showOutput() {
		if( this.splitPane.getRightComponent().getWidth() < 75 )
			this.splitPane.setDividerLocation( this.splitPane.getWidth() * 62 / 100 );
	}

}
