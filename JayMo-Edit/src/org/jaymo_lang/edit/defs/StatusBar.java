/*******************************************************************************
 * Copyright (C): 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit.defs;

import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.SwingConstants;


/**
 * @author Michael Nitsche
 * @created 28.10.2020
 */
public class StatusBar extends JLabel {

	private static final long serialVersionUID = -7236082049980028092L;


	public StatusBar() {
		super();

//		this.setEditable(false);
//		this.setMargin(new Insets(5, 5, 5, 5));
		this.setBorder( null );
//		this.setSize(50, 50);
		this.setPreferredSize( new Dimension( 200, 25 ) );
//		this.setOpaque(false);
		this.setHorizontalAlignment( SwingConstants.LEFT );
		this.setVerticalAlignment( SwingConstants.TOP );
//		this.setBounds(20, 20, 20, 20);

		final Font font = new Font( Font.SANS_SERIF, Font.PLAIN, 13 );
		this.setFont( font );
	}

	public void setMessage( final String t ) {
		this.setText( "  " + t );
	}

}
