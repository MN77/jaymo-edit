/*******************************************************************************
 * Copyright (C): 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit.editor.styler;

import java.util.ArrayList;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Style;
import javax.swing.text.StyledDocument;

import org.jaymo_lang.edit.defs.FileEditor;
import org.jaymo_lang.edit.editor.styler.styles.SECTION_TYPE;
import org.jaymo_lang.edit.editor.styler.styles.Styles;

import de.mn77.base.data.group.Section;


/**
 * @author Michael Nitsche
 * @created 28.10.2020
 */
public class Styler implements DocumentListener, I_Styler {

	private final FileEditor     editor;
	private final StyledDocument doc;
	private final Styles         styles;
//	private TextUpdate lastUpdate = null;
	private boolean       scanNeeded = false;
	private final Scanner scanner;


	public Styler( final FileEditor editor ) {
		this.editor = editor;
		this.doc = (StyledDocument)editor.getDocument();
		this.doc.addDocumentListener( this );

		this.styles = new Styles();
		this.styles.initStyles( editor );

		this.scanner = new Scanner( this.doc, this.styles );
	}

	@Override
	public void changedUpdate( final DocumentEvent e ) {
//		this.lastUpdate = new TextUpdate(e.getOffset(), e.getLength(), e.getLength());
//		this.scanNeeded = true;
//		this.editor.repaint();
		// Wird eigentlich nicht gebraucht. Alle Änderungen im Text laufen über insert/remove
	}

	@Override
	public void insertUpdate( final DocumentEvent e ) {
//		this.lastUpdate = new TextUpdate(e.getOffset(), 0, e.getLength());
		this.scanNeeded = true;
		this.editor.setChanged( true );
		this.editor.repaint();
	}

	public void paintFind( String find ) {
//		this.lockRepaint = true;

		// Clear highlightings
		this.scanNeeded = true;
		this.scanner.update();

		final StyledDocument doc = this.editor.getStyledDocument();
		final String text = this.editor.getText().toLowerCase();
		find = find.toLowerCase();
		final int fLen = find.length();

		final Style style = this.styles.styles[SECTION_TYPE.FOUND.ordinal()];

		for( int i = 0; i < text.length() - fLen; i++ )
			if( text.startsWith( find, i ) ) {
				doc.setCharacterAttributes( i, fLen, style, true );
				i += fLen;
			}
		if( text.endsWith( find ) )
			doc.setCharacterAttributes( text.length() - fLen, fLen, style, true );

		this.scanNeeded = false; // Important!
	}

	public void paintOccurrences( final ArrayList<Section> marks ) {
		// Clear highlightings
		this.scanNeeded = true;
		this.scanner.update();

		final StyledDocument doc = this.editor.getStyledDocument();
		final Style style = this.styles.styles[SECTION_TYPE.OCCUR.ordinal()];

		for( final Section mark : marks )
			doc.setCharacterAttributes( mark.start, mark.end, style, false );

		this.scanNeeded = false; // Important!
	}

	@Override
	public void paintStyles() {
//		this.doc.getText(offset, length, this.text);
//		this.doc.setCharacterAttributes(this.position, length, this.tokenTypes.styles[type.ordinal()], false);

//		if(this.lastUpdate != null) {
//			this.scanner.update(this.lastUpdate);
//			this.lastUpdate = null;
//		}

		if( this.scanNeeded ) {
			this.scanner.update();
			this.scanNeeded = false;
		}
	}

	@Override
	public void removeUpdate( final DocumentEvent e ) {
//		this.lastUpdate = new TextUpdate(e.getOffset(), e.getLength(), 0);
		this.scanNeeded = true;
		this.editor.setChanged( true );
		this.editor.repaint();
	}

	public void repaint() {
		this.scanNeeded = true;
		this.paintStyles();
	}

}
