/*******************************************************************************
 * Copyright (C): 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit.editor.styler.styles;

import java.awt.Color;
import java.awt.Font;

import javax.swing.text.Style;
import javax.swing.text.StyleConstants;

import org.jaymo_lang.edit.editor.CodeEditor;

import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 28.10.2020
 */
public class Styles {

	public final Style[] styles = new Style[SECTION_TYPE.values().length];
	private CodeEditor   editor = null;


	public void initStyles( final CodeEditor editor ) {
		this.editor = editor;
		final Color blue = new Color( 50, 50, 255 ); // Color(53,45,230)		Color(0,0,255);
//		final Color green = new Color(15, 131, 60);
		final Color green = new Color( 31, 122, 71 );
		final Color black = Color.BLACK; // (0, 0, 0);
		final Color dark_gray = Color.DARK_GRAY; // (64, 64, 64);
		final Color magenta = new Color( 230, 50, 230 );// 255,0,255
		final Color red = new Color( 255, 0, 0 );
		final Color dark_red = new Color( 164, 0, 0 );
//		final Color yellow = new Color(150, 150, 0);
		final Color orange = new Color( 242, 121, 0 ); //new Color(220,124,0);//new Color(230,114,0);//new Color(185, 121, 2); // new Color(233,151,23)
		final Color cyan = new Color( 0, 196, 196 ); //new Color(220,124,0);//new Color(230,114,0);//new Color(185, 121, 2); // new Color(233,151,23)
//		final Color purple = new Color(106,90,205);
		final Color light_gray = new Color( 175, 175, 175 ); // 159 ist zu stark
//		final Color light_gray = new Color(184, 184, 184); //Color.LIGHT_GRAY; // Pluma: (192, 192, 192)
//		final Color light_gray = new Color(192, 192, 192);	// Pluma	// zu schwach
		final Color light_yellow = new Color( 255, 255, 0 );
		final Color light_green = new Color( 179, 255, 220 ); //new Color(207, 255, 207);
//		final Color petrol = new Color(0,164, 164);
//		final Color lila = new Color(127,0,255);
		// brown = new Color(106,69,0)

		//=====================================================================================//

//		this.setEmpty(StyleType.IGNORE);
		this.setStyle( SECTION_TYPE.IGNORE, black );
		this.setEmpty( SECTION_TYPE.LINEBREAK );
		this.setEmpty( SECTION_TYPE.WHITESPACE );
		this.setEmpty( SECTION_TYPE.UNKNOWN_WORD ); //TODO: BLACK?

//		this.setImage(StyleType.WHITESPACE_LEADING, "org/jmo_lang/whitespace_leading2.png");
		this.setBackground( SECTION_TYPE.WHITESPACE_LEADING, red );
		this.setBackground( SECTION_TYPE.ERROR, red );
//		this.setImage(StyleType.TABULATOR, "org/jmo_lang/tab2.png");
//		this.setBackground(StyleType.TABULATOR, new Color(247,247,247));
		this.setEmpty( SECTION_TYPE.TABULATOR );

		this.setStyle( SECTION_TYPE.FUNCTION, black );
		this.setStyle( SECTION_TYPE.LOOP_FUNCTION, black, Font.BOLD );
		this.setStyle( SECTION_TYPE.CHAR, magenta );
		this.setStyle( SECTION_TYPE.STRING, magenta );
		this.setStyle( SECTION_TYPE.NUMBER, magenta );
		this.setStyle( SECTION_TYPE.ATOMIC_LITERAL, magenta );
		this.setStyle( SECTION_TYPE.PUNCTUATION, dark_gray );
		this.setBackground( SECTION_TYPE.COMMAND, black, light_gray );
//		this.setBackground(StyleType.DEFINITION, dark_gray, light_blue);
		this.setStyle( SECTION_TYPE.DEFINITION, dark_red, Font.BOLD );
		this.setStyle( SECTION_TYPE.BRACKET, dark_gray );
		this.setStyle( SECTION_TYPE.MAGIC_SHORT, cyan ); //, Font.BOLD
		this.setStyle( SECTION_TYPE.MAGIC_LONG, cyan );//, Font.BOLD);
		this.setStyle( SECTION_TYPE.EVENT, cyan ); // , Font.BOLD
		this.setStyle( SECTION_TYPE.TYPE, green, Font.BOLD );
		this.setStyle( SECTION_TYPE.CONST, blue, Font.BOLD ); //, Font.BOLD
		this.setStyle( SECTION_TYPE.VAR, blue );
		this.setStyle( SECTION_TYPE.PARSERSETTING, orange ); //, Font.BOLD

		this.setStyle( SECTION_TYPE.COMMENT_LINE, light_gray, Font.ITALIC );
		this.setStyle( SECTION_TYPE.COMMENT_BLOCK, light_gray, Font.ITALIC );
		this.setStyle( SECTION_TYPE.COMMENT_SCRIPT, light_gray, Font.ITALIC );

		this.setBackground( SECTION_TYPE.FOUND, light_yellow );
		this.setBackground( SECTION_TYPE.OCCUR, light_green );

		//=====================================================================================//

		// Check, if everything set:
		for( int i = 0; i < this.styles.length; i++ )
			if( this.styles[i] == null ) {
				MOut.temp( "Missing style for: " + SECTION_TYPE.values()[i].name() );
				Err.invalid( "Missing style" );
			}
	}

	private void setBackground( final SECTION_TYPE type, final Color color ) {
		final Style style = this.editor.addStyle( type.name(), null );
		StyleConstants.setBackground( style, color );
		this.styles[type.ordinal()] = style;
	}

	private void setBackground( final SECTION_TYPE type, final Color front, final Color back ) {
		final Style style = this.editor.addStyle( type.name(), null );
		StyleConstants.setForeground( style, front );
		StyleConstants.setBackground( style, back );
		this.styles[type.ordinal()] = style;
	}

	private void setEmpty( final SECTION_TYPE type ) {
		final Style style = this.editor.addStyle( type.name(), null );
		this.styles[type.ordinal()] = style;
	}

	private void setStyle( final SECTION_TYPE type, final Color color ) {
		final Style style = this.editor.addStyle( type.name(), null );
		StyleConstants.setForeground( style, color );
		this.styles[type.ordinal()] = style;
	}

	private void setStyle( final SECTION_TYPE type, final Color color, final int fontStyle ) {
		final Style style = this.editor.addStyle( type.name(), null );
		StyleConstants.setForeground( style, color );
		if( (fontStyle & Font.BOLD) != 0 )
			StyleConstants.setBold( style, true );
		if( (fontStyle & Font.ITALIC) != 0 )
			StyleConstants.setItalic( style, true );
		this.styles[type.ordinal()] = style;
	}

//	private void setImage(final StyleType type, final String imgpath) {
//		final ImageIcon img = new ImageIcon(this.getClass().getClassLoader().getResource(imgpath)); //"org/jmo_lang/icon256.png"
//		final Style style = this.editor.addStyle(type.name(), null);
//		StyleConstants.setIcon(style, img);
//		this.styles[type.ordinal()] = style;
//	}

}
