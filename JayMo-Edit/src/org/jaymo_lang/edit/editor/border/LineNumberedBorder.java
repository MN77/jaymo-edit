/*******************************************************************************
 * Copyright (C): 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit.editor.border;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.io.InputStream;

import javax.swing.border.AbstractBorder;

import org.jaymo_lang.edit.Main_JayMo_Edit;
import org.jaymo_lang.edit.editor.CodeEditor;

import de.mn77.base.data.util.Lib_String;
import de.mn77.base.error.Err;
import de.mn77.base.sys.file.Lib_Jar;


public class LineNumberedBorder extends AbstractBorder {

	private static final long  serialVersionUID    = -3812536735962506061L;
	private static final Color COLOR_NUMBERS       = Color.DARK_GRAY;//Color.black; //new Color(224, 224, 224);//Color.DARK_GRAY;
	private static final Color COLOR_SEPARATORLINE = Color.LIGHT_GRAY;//Color.black; //Color.LIGHT_GRAY;
	private static final Color COLOR_BACKGROUND    = new Color( 238, 238, 238 );//Color.LIGHT_GRAY; //new Color(127, 127, 127);
	private static final int   offsetEditX         = 3;
	private static final int   MAX_LINENR          = 999;
	private final Font         font;


	private static String padLabel( final int lineNumber, final int length ) {
		final int spaces = 3;
		return Lib_String.sequence( ' ', spaces ) + lineNumber + ' ';
	}

	public LineNumberedBorder() {
		Font f = null;

		try {
			final InputStream is = Lib_Jar.getStream( Main_JayMo_Edit.BORDER_FONT_FILE );
			f = Font.createFont( Font.TRUETYPE_FONT, is ).deriveFont( Main_JayMo_Edit.FONT_SIZE );
		}
		catch( final Exception e ) {
			Err.show( e );
		}

		this.font = f;
	}

	@Override
	public Insets getBorderInsets( final Component c ) {
		return this.getBorderInsets( c, new Insets( 0, 0, 0, 0 ) );
	}

	@Override
	public Insets getBorderInsets( final Component c, final Insets insets ) {
		if( c instanceof CodeEditor )
			insets.left = this.lineNumberWidth( (CodeEditor)c ) + LineNumberedBorder.offsetEditX; //Add space to delimiter line
//		insets.top = insets.top+5;	// Top offset
		return insets;
	}

	@Override
	public void paintBorder( final Component c, final Graphics g, final int x, final int y, final int width, final int height ) {
		// Activate antialias
		final Graphics2D g2d = (Graphics2D)g;
		g2d.setRenderingHint( RenderingHints.KEY_TEXT_ANTIALIASING,
			RenderingHints.VALUE_TEXT_ANTIALIAS_GASP );
//	        RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
//			RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

		final java.awt.Rectangle clip = g.getClipBounds();
		final FontMetrics fm = g.getFontMetrics();
		final int fontHeight = fm.getHeight();
		int ybaseline = y + fm.getAscent();
		int startingLineNumber = clip.y / fontHeight + 1;
		if( ybaseline < clip.y )
			ybaseline = y + startingLineNumber * fontHeight - (fontHeight - fm.getAscent());

		final CodeEditor jta = (CodeEditor)c;
//		int yend = ybaseline + height;				// Draw up to the bottom of Component
		int yend = jta.getLineCount() * fontHeight; // Draw only used lines

		if( yend > y + height )
			yend = y + height;

		final int lineWidth = this.lineNumberWidth( jta );
		int lnxstart = x + lineWidth;

		// Draw background
		g.setColor( LineNumberedBorder.COLOR_BACKGROUND );
		g.fillRect( 0, 0, lineWidth, yend * lineWidth );

		// Draw numbers
		g.setColor( LineNumberedBorder.COLOR_NUMBERS );
		g.setFont( this.font );
		final int length = ("" + jta.getLineCount() + 1).length();

		while( ybaseline < yend ) {
			final String label = LineNumberedBorder.padLabel( startingLineNumber, length );
			g.drawString( label, lnxstart - fm.stringWidth( label ), ybaseline );

			ybaseline += fontHeight;
			startingLineNumber++;
		}

		// Draw separator line
		g.setColor( LineNumberedBorder.COLOR_SEPARATORLINE );
		lnxstart = x + lineWidth - 1;
		g.drawLine( lnxstart, 0, lnxstart, height );
	}

	private int lineNumberWidth( final CodeEditor textArea ) {
		int lineCount = textArea.getLineCount() + 1;
		lineCount = Math.max( lineCount, LineNumberedBorder.MAX_LINENR );
		return textArea.getFontMetrics( textArea.getFont() ).stringWidth(
			lineCount + " " );
	}

}

