/*******************************************************************************
 * Copyright (C): 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit.editor.funcs;

import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JTextPane;

import de.mn77.base.data.group.Group2;
import de.mn77.base.data.struct.table.type.TypeTable2;
import de.mn77.lib.ui.A_History;


/**
 * @author Michael Nitsche
 * @created 17.03.2021
 */
public class Fn_UndoRedo extends A_History {

	private static final int            MAX           = 30;
	private TypeTable2<String, Integer> history;
	private JTextPane                   tp;
	private final int[]                 caretPosition = { 0 };


	public Fn_UndoRedo() {
		super();
	}

	public void init( final JTextPane editor ) {
		this.tp = editor;

		editor.addCaretListener( e -> Fn_UndoRedo.this.caretPosition[0] = e.getDot() );

		editor.addKeyListener( new KeyListener() {

			public void keyPressed( final KeyEvent e ) {}

			public void keyReleased( final KeyEvent e ) {
				final int mod = e.getModifiersEx();
				final int kc = e.getKeyCode();

				if( kc == 90 && mod == InputEvent.CTRL_DOWN_MASK ) // Strg + Z
					Fn_UndoRedo.this.undo();
				else if( kc == 90 && mod == InputEvent.CTRL_DOWN_MASK + InputEvent.SHIFT_DOWN_MASK || kc == 89 && mod == InputEvent.CTRL_DOWN_MASK ) // Strg + Shift + Z
					Fn_UndoRedo.this.redo();
				else if( kc != KeyEvent.CHAR_UNDEFINED )
					Fn_UndoRedo.this.add();
			}

			public void keyTyped( final KeyEvent e ) {
//				// Not useful, this is executed BEFORE getText will be updated
			}

		} );

		this.add(); // Add empty state
	}

	@Override
	protected boolean internalAdd() {
//		this.caretPosition=new int[] {this.tp.getCaretPosition()};
		final String text = this.tp.getText();

		// Prevent doubles
		final int hSize = this.history.size();
		if( hSize > 0 && this.history.get( hSize - 1 ).o1.equals( text ) )
			return false;

		this.history.add( text, this.caretPosition[0] ); // Use caret position BEFORE deletion

//		MOut.temp("ADD "+ this.caretPosition[0]+": "+text);
		return true;
	}

	@Override
	protected void internalClear() {
		this.history = new TypeTable2<>( String.class, Integer.class );
		if( this.tp != null )
			this.add(); // Add empty state
	}

	@Override
	protected int internalGetMax() {
		return Fn_UndoRedo.MAX;
	}

	@Override
	protected int internalGetSize() {
		return this.history.size();
	}

	@Override
	protected void internalRemoveFirst() {
		this.history.remove( 1 );
	}

	@Override
	protected void internalRemoveLast() {
		this.history.removeLast();
	}

	@Override
	protected void internalSet( final int pos ) {
		final Group2<String, Integer> g = this.history.get( pos );
		this.tp.setText( g.o1 );
		this.tp.setCaretPosition( g.o2 );
	}

}
