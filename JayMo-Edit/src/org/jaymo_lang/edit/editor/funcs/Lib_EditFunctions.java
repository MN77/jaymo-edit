/*******************************************************************************
 * Copyright (C): 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit.editor.funcs;

import de.mn77.base.data.group.Section;


/**
 * @author Michael Nitsche
 * @created 19.03.2021
 */
public class Lib_EditFunctions {

	public static Section getLines( final String text, int start, int end ) {
		final int len = text.length();
		if( len == 0 )
			return null;

		// Correct order or start/end
		if( start > end ) {
			final int buf = start;
			start = end;
			end = buf;
		}

		// --- Search prior line break ---

		int x = start <= 0 ? 0 : start;

		// Correction if there's no selection (start will be after the char)
		if( start == end && x > 0 )
			x--;

		char c = text.charAt( x );

		while( c != '\n' && x > 0 ) {
			x--;
			c = text.charAt( x );
		}

		// Don't select prior \n
		if( c == '\n' && start != 0 )
			x++;

		// --- Search next line break ---

		int y = end >= len ? len - 1 : end;
		c = text.charAt( y );

		while( c != '\n' && y < len - 1 ) {
			y++;
			c = text.charAt( y );
		}

//		MOut.temp(start+"/"+end+"  -->  "+x+'/'+y);	//, text.substring(x, y)
		return new Section( x, y );
	}

}
