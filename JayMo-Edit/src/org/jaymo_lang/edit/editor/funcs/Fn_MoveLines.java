/*******************************************************************************
 * Copyright (C): 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit.editor.funcs;

import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import org.jaymo_lang.edit.editor.CodeEditor;

import de.mn77.base.data.constant.DIRECTION_VERTICAL;
import de.mn77.base.data.group.Section;


/**
 * @author Michael Nitsche
 * @created 11.04.2021
 */
public class Fn_MoveLines {

	public void init( final CodeEditor editor ) {
		editor.addKeyListener( new KeyListener() {

			public void keyPressed( final KeyEvent e ) {}

			public void keyReleased( final KeyEvent e ) {
				if( e.getModifiersEx() == InputEvent.ALT_DOWN_MASK && e.getKeyCode() == 38 ) // ALT + UP
					Fn_MoveLines.this.iMoveLines( editor, DIRECTION_VERTICAL.UP );
				if( e.getModifiersEx() == InputEvent.ALT_DOWN_MASK && e.getKeyCode() == 40 ) // ALT + DOWN
					Fn_MoveLines.this.iMoveLines( editor, DIRECTION_VERTICAL.DOWN );
			}

			public void keyTyped( final KeyEvent e ) {}

		} );
	}

	private void iMoveLines( final CodeEditor editor, final DIRECTION_VERTICAL dir ) {
		final int cursorPos = editor.getCaretPosition();
		final int markEnd = editor.getCaret().getMark();
//		MOut.temp(cursorPos, markEnd);
		final String text = editor.getText();

		final Section selection = Lib_EditFunctions.getLines( text, cursorPos, markEnd );

		if( selection != null ) {
			if( selection.start == 0 || selection.end >= text.length() )
				return;

			String textToMove = text.substring( selection.start, selection.end + 1 );
			if( textToMove.length() > 0 && !textToMove.endsWith( "\n" ) )
				textToMove += '\n';

			final int cursorMoved = dir == DIRECTION_VERTICAL.UP ? selection.start - 1 : selection.end + 1;
			final Section dodge = Lib_EditFunctions.getLines( text, cursorMoved, cursorMoved );

			final String textToDodge = text.substring( dodge.start, dodge.end + 1 );
			final int dodgeLen = dodge.end - dodge.start + 1;

			if( dir == DIRECTION_VERTICAL.UP ) {
				editor.select( selection.start, selection.end + 1 );
				editor.replaceSelection( "" );
				editor.setCaretPosition( dodge.start );
				editor.replaceSelection( textToMove );

				editor.setCaretPosition( cursorPos - dodgeLen );
				if( markEnd > cursorPos )
					editor.select( cursorPos - dodgeLen, markEnd - dodgeLen );
				if( markEnd < cursorPos )
					editor.select( markEnd - dodgeLen, cursorPos - dodgeLen );
			}
			else {
				editor.select( dodge.start, dodge.end + 1 );
				editor.replaceSelection( "" );
				editor.setCaretPosition( selection.start );
				editor.replaceSelection( textToDodge );

				final int newCaretPosition = Math.min( cursorPos + dodgeLen, editor.getText().length() - 1 );

				editor.setCaretPosition( newCaretPosition );
				if( markEnd > cursorPos )
					editor.select( newCaretPosition, markEnd + dodgeLen );
				if( markEnd < cursorPos )
					editor.select( markEnd + dodgeLen, newCaretPosition );
			}

			editor.getHistory().add();
		}
	}

}
