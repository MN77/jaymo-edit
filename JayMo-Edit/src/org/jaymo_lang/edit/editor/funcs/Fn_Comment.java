/*******************************************************************************
 * Copyright (C): 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit.editor.funcs;

import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

import javax.swing.text.JTextComponent;

import org.jaymo_lang.edit.editor.CodeEditor;

import de.mn77.base.data.convert.ConvertString;
import de.mn77.base.data.group.Group2;
import de.mn77.base.data.group.Section;


/**
 * @author Michael Nitsche
 * @created 2021-04-25
 */
public class Fn_Comment {

	public void init( final CodeEditor editor ) {
		editor.addKeyListener( new KeyListener() {

			public void keyPressed( final KeyEvent e ) {

				// CTRL + /, CTRL + #
				if( e.getModifiersEx() == InputEvent.CTRL_DOWN_MASK && (e.getKeyCode() == 55 || e.getKeyCode() == 520) ) {
					e.consume();
					final boolean b = Fn_Comment.this.iComment( editor );
					if( b )
						editor.getHistory().add();
				}
			}

			public void keyReleased( final KeyEvent e ) {}

			public void keyTyped( final KeyEvent e ) {}

		} );
	}

	protected boolean iComment( final JTextComponent tp ) {
		final int cursorPos = tp.getCaretPosition();
		final int x = tp.getCaret().getMark();

		final String text = tp.getText();

		if( text.length() == 0 ) // This may be at the end of the text.
			return false;

		final Section selection = Lib_EditFunctions.getLines( text, cursorPos, x );

		if( selection != null ) {
			if( selection.end < selection.start ) // This happens at the end of the text.
				return false;

			final String part = text.substring( selection.start, selection.end );
			final List<String> lines = ConvertString.toLines( part );
			final boolean isCommented = this.isCommented( lines );

			final Group2<String, Integer> pa = isCommented ? this.iReplaceRemove( lines ) : this.iReplaceAdd( lines );

			tp.select( selection.start, selection.end );
			tp.replaceSelection( pa.o1 );

			tp.select( cursorPos + pa.o2, cursorPos + pa.o2 );
			return true;
		}

		return false;
	}

	private Group2<String, Integer> iReplaceAdd( final List<String> lines ) {
		final StringBuilder sb = new StringBuilder();

		for( final String line : lines ) {
			sb.append( "# " );
			sb.append( line );
			sb.append( '\n' );
		}
		sb.deleteCharAt( sb.length() - 1 );
		return new Group2<>( sb.toString(), lines.size() * 2 );
	}

	private Group2<String, Integer> iReplaceRemove( final List<String> lines ) {
		final StringBuilder sb = new StringBuilder();
		int changed = 0;

		for( final String line : lines ) {
			final String line2 = line.replaceFirst( "[ \\t]*#[ ]?", "" );
			changed -= line.length() - line2.length();
			sb.append( line2 );
			sb.append( '\n' );
		}
		sb.deleteCharAt( sb.length() - 1 );
		return new Group2<>( sb.toString(), changed );
	}

	private boolean isCommented( final List<String> lines ) {
		final String line1 = lines.get( 0 ).trim();
		final boolean searchCommented = line1.length() > 0 && line1.charAt( 0 ) == '#';

		for( String line : lines ) {
			line = line.trim();
			if( line.length() > 0 ) // Ignore empty lines
				if( line.charAt( 0 ) == '#' != searchCommented )
					return false;
		}

		return searchCommented;
	}

}
