/*******************************************************************************
 * Copyright (C): 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit.editor.funcs;

import javax.swing.text.BadLocationException;
import javax.swing.text.JTextComponent;

import org.jaymo_lang.util.Lib_Parser;

import de.mn77.base.data.group.Group2;
import de.mn77.base.data.group.Group3;
import de.mn77.base.data.group.Section;
import de.mn77.base.data.util.Lib_String;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 23.03.2021
 */
public class Lib_IndentOutdent {

	/**
	 * @return true if text modified
	 */
	public static boolean indent( final JTextComponent tp, final boolean force ) {
		int cursorPos = tp.getCaretPosition();
		int x = tp.getCaret().getMark();
		final String text = tp.getText();

		if( cursorPos == x && !force ) // Only do anything, when some Text is selected
			return Lib_IndentOutdent.iAutoIndent( cursorPos, text, tp );

		if( text.length() == 0 ) // This may be at the end of the text.
			return false;

		final Section selection = Lib_EditFunctions.getLines( text, cursorPos, x );

		if( selection != null ) {
			if( selection.end < selection.start ) // This happens at the end of the text.
				return false;

			String part = text.substring( selection.start, selection.end );
			final Group2<String, Integer> pa = Lib_IndentOutdent.iIndentReplace( part );// +'\n';
			part = '\t' + pa.o1;

			tp.select( selection.start, selection.end );
			tp.replaceSelection( part );

			// Correct order
			if( cursorPos > x ) {
				final int buf = cursorPos;
				cursorPos = x;
				x = buf;
			}

			tp.select( cursorPos + 1, x + pa.o2 + 1 );
//			tp.setCaretPosition(cursorPos+1);	// Doesn't work

			return true;
		}
		return false;
	}

	public static boolean outdent( final JTextComponent tp ) {
		int cursorPos = tp.getCaretPosition();
		int x = tp.getCaret().getMark();

		final String text = tp.getText();

		final Section selection = Lib_EditFunctions.getLines( text, cursorPos, x );

		if( selection != null ) {
			String part = text.substring( selection.start, selection.end );
			final Group3<String, Integer, Boolean> pa = Lib_IndentOutdent.iOutdentReplace( part );
			part = pa.o1;

			tp.select( selection.start, selection.end ); //+1
			tp.replaceSelection( part );

			// Correct order
			if( cursorPos > x ) {
				final int buf = cursorPos;
				cursorPos = x;
				x = buf;
			}

			if( pa.o3 )
				cursorPos--;
			tp.select( cursorPos, x - pa.o2 );
//			tp.setCaretPosition(cursorPos+1);	// Doesn't work

			return true;
		}
		return false;
	}

	private static boolean iAutoIndent( final int cursorPos, final String text, final JTextComponent tp ) {
		if( text.length() == 0 )
			return false;

		int currentEnd = cursorPos;

		while( currentEnd < text.length() ) {
			final char cx = text.charAt( currentEnd );
			if( cx == '\n' )
				break;
			if( cx != ' ' && cx != '\t' )
				return false;
			currentEnd++;
		}

		int currentStart = cursorPos - 1;
		while( currentStart > 0 && text.charAt( currentStart ) != '\n' )
			currentStart--;
		currentStart++; // Move from \n to first char

		final String currentText = text.substring( currentStart, currentEnd );

		if( currentText.trim().length() > 0 ) // No autoindent if the line already has some content
			return false;

		final int currentIndent = Lib_Parser.getDepth( currentText ); // Only leading tabs will be parsed

		int beforeStart = currentStart - 2; // -1 == Linebreak, -2 = last char
		if( beforeStart < 0 ) // Happens, when first line is empty
			beforeStart = 0;

		// Move up to the end of the last line with content
		while( beforeStart > 0 && "\n \t".indexOf( text.charAt( beforeStart ) ) != -1 ) // Jump over all \n,\t,' '
			beforeStart--;
		// Move to the first char of the line
		while( beforeStart > 0 && text.charAt( beforeStart ) != '\n' )
			beforeStart--;

		final String beforeText = text.substring( beforeStart + 1 );
		final int beforeIndent = Lib_Parser.getDepth( beforeText ); // Only leading tabs will be parsed

		if( currentIndent >= beforeIndent )
			return false;

		final int indentDiff = beforeIndent - currentIndent;

		try {
			tp.getDocument().insertString( cursorPos, Lib_String.sequence( '\t', indentDiff ), null );
		}
		catch( final BadLocationException err ) {
			Err.show( err );
		}
		return true;
	}

	private static Group2<String, Integer> iIndentReplace( final String s ) {
		int hits = 0;
		boolean openString = false;
		boolean openChar = false;

		final StringBuilder sb = new StringBuilder( s.length() + 10 );

		for( int idx = 0; idx < s.length(); idx++ ) {
			char c = s.charAt( idx );

			if( (openString || openChar) && c == '\\' ) {
				sb.append( c );
				idx++;
				c = s.charAt( idx );
			}
			else if( c == '"' )
				openString = !openString;
			else if( c == '\'' )
				openChar = !openChar;
			else if( openString || openChar ) {}
			else if( c == '\n' ) {
				hits++;
				sb.append( c );
				c = '\t';
			}

			sb.append( c );
		}

		return new Group2<>( sb.toString(), hits );
	}

	private static Group3<String, Integer, Boolean> iOutdentReplace( final String s ) {
		int hits = 0;
		boolean openString = false;
		boolean openChar = false;
		boolean first = true;
		boolean hitFirst = false;

		final StringBuilder sb = new StringBuilder( s.length() );

		for( int idx = 0; idx < s.length(); idx++ ) {
			char c = s.charAt( idx );

			if( first && c == '\t' ) {
				hits++;
				hitFirst = true;
				first = false;
				continue;
			}
			first = false;

			if( (openString || openChar) && c == '\\' ) {
				sb.append( c );
				idx++;
				c = s.charAt( idx );
			}
			else if( c == '"' )
				openString = !openString;
			else if( c == '\'' )
				openChar = !openChar;
			else if( openString || openChar ) {}
			else if( s.startsWith( "\n\t", idx ) ) {
				hits++;
				idx++;
			}

			sb.append( c );
		}

		return new Group3<>( sb.toString(), hits, hitFirst );
	}

}
