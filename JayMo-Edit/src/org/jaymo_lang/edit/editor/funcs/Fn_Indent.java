/*******************************************************************************
 * Copyright (C): 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit.editor.funcs;

import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import org.jaymo_lang.edit.editor.CodeEditor;


/**
 * @author Michael Nitsche
 * @created 2021-03-17
 */
public class Fn_Indent {

	public void init( final CodeEditor editor ) {
		editor.addKeyListener( new KeyListener() {

			public void keyPressed( final KeyEvent e ) {

				// Tab / Shift+Tab
				if( e.getKeyCode() == 9 ) {
					final int mod = e.getModifiersEx();

					if( mod == 0 ) {
						if( Fn_Indent.this.iIndent( editor ) )
							e.consume();
					}
					else if( mod == InputEvent.SHIFT_DOWN_MASK ) {
						e.consume();
						Fn_Indent.this.iOutdent( editor );
					}
				}
			}

			public void keyReleased( final KeyEvent e ) {}

			public void keyTyped( final KeyEvent e ) {}

		} );
	}

	protected boolean iIndent( final CodeEditor editor ) {
		final boolean b = Lib_IndentOutdent.indent( editor, false );
		if( b )
			editor.getHistory().add();
		return b;
	}

	protected void iOutdent( final CodeEditor editor ) {
		final boolean b = Lib_IndentOutdent.outdent( editor );
		if( b )
			editor.getHistory().add();
	}

}
