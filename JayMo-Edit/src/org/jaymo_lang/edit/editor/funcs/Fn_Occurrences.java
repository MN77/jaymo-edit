/*******************************************************************************
 * Copyright (C): 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit.editor.funcs;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import org.jaymo_lang.edit.editor.CodeEditor;
import org.jaymo_lang.edit.refactor.JayMo_Refactor;

import de.mn77.base.data.group.Section;


/**
 * @author Michael Nitsche
 * @created 2021-04-12
 */
public class Fn_Occurrences {

	public void init( final CodeEditor editor ) {
		editor.addMouseListener( new MouseListener() {

			public void mouseClicked( final MouseEvent e ) {
				final ArrayList<Section> mark = JayMo_Refactor.occurrences( editor.getText(), editor.getCaretPosition() );
				editor.occurrences( mark );
			}

			public void mouseEntered( final MouseEvent e ) {}

			public void mouseExited( final MouseEvent e ) {}

			public void mousePressed( final MouseEvent e ) {}

			public void mouseReleased( final MouseEvent e ) {}

		} );
	}

}
