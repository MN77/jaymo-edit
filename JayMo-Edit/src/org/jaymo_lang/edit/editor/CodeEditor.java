/*******************************************************************************
 * Copyright (C): 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit.editor;

/**
 * @author Michael Nitsche
 * @created 28.10.2020
 */
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

import javax.swing.JTextPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Position;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.TabSet;
import javax.swing.text.TabStop;

import org.jaymo_lang.edit.editor.border.LineNumberedBorder;
import org.jaymo_lang.edit.editor.funcs.Fn_Comment;
import org.jaymo_lang.edit.editor.funcs.Fn_DelLine;
import org.jaymo_lang.edit.editor.funcs.Fn_Indent;
import org.jaymo_lang.edit.editor.funcs.Fn_MoveLines;
import org.jaymo_lang.edit.editor.funcs.Fn_Occurrences;
import org.jaymo_lang.edit.editor.funcs.Fn_UndoRedo;
import org.jaymo_lang.edit.editor.styler.I_Styler;
import org.jaymo_lang.util.Lib_Parser;

import de.mn77.base.data.group.Section;
import de.mn77.base.data.util.Lib_String;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 28.03.2021
 * @implNote
 *           https://www.javatips.net/api/mspsim-master/se/sics/mspsim/ui/SourceViewer.java
 */
public class CodeEditor extends JTextPane {

	private static final long serialVersionUID = -1801145479677890566L;

	public static final boolean DEMO = false;

	private static final Color LINE_HIGHLIGHT_COLOR = new Color( 238, 238, 238, 255 ); // (232,242,254, 255) ist sehr blass  //240,240,255 Ok wäre auch noch 235


	private I_Styler          style         = null;
	private int               caretYcurrent = 0;
	private int               currentLineHeight; // Needed for highlight current line. Adjust if size changed. Check size of highlighted line at start!
	private final Fn_UndoRedo history       = new Fn_UndoRedo();


	public CodeEditor( final int font_char_width, final int font_line_hight ) {
		this.currentLineHeight = font_line_hight;
		this.setEditable( true );
//		this.setMargin(new Insets(5, 5, 5, 5));	// Tut nicht
		this.setOpaque( false ); //Ansonsten wird die aktuelle Zeile nicht eingefärbt!
		this.setBorder( new LineNumberedBorder() );
//		this.setContentType("text/plain");	// Default
//		this.putClientProperty(SwingUtilities2.AA_TEXT_PROPERTY_KEY, null);	// Kein Antialias
//		this.putClientProperty(com.sun.java.swing.SwingUtilities2.AA_TEXT_PROPERTY_KEY, Boolean.TRUE);
//		System.setProperty("awt.useSystemAAFontSettings","false");// and "lcd";	// This makes no difference

		this.history.init( this );
		new Fn_DelLine().init( this );
		new Fn_Indent().init( this );
		new Fn_MoveLines().init( this );
		new Fn_Occurrences().init( this );
		new Fn_Comment().init( this );

		// --- TABULATOR ---
		final TabStop[] tabs = new TabStop[20]; // 20 sollten reichen
//      TabStop.ALIGN_RIGHT, TabStop.ALIGN_LEFT, TabStop.ALIGN_CENTER,  TabStop.ALIGN_DECIMAL
		final int tabWidth = font_char_width * 4; // 4 Chars
		for( int i = 0; i < tabs.length; i++ )
			tabs[i] = new TabStop( (i + 1) * tabWidth, TabStop.ALIGN_LEFT, TabStop.LEAD_NONE );
		final TabSet tabset = new TabSet( tabs );
		final StyleContext sc = StyleContext.getDefaultStyleContext();
		final AttributeSet aset = sc.addAttribute( SimpleAttributeSet.EMPTY,
			StyleConstants.TabSet, tabset );
		this.setParagraphAttributes( aset, false );


		// --- highlight selected line ---
		this.addCaretListener( e -> {
			final int caret = CodeEditor.this.getCaretPosition();
			if( caret >= 0 )
				try {
//					final Rectangle r = CodeEditor.this.getUI().modelToView(CodeEditor.this, caret); //Deprecated since Java 9
					final Rectangle2D r = CodeEditor.this.getUI().modelToView2D( CodeEditor.this, caret, Position.Bias.Forward );
					if( CodeEditor.this.currentLineHeight > 0 )
						CodeEditor.this.repaint( 0, CodeEditor.this.caretYcurrent, CodeEditor.this.getWidth(), CodeEditor.this.currentLineHeight );

					if( r != null && r.getHeight() > 0 ) {
						CodeEditor.this.caretYcurrent = (int)r.getY();
						CodeEditor.this.currentLineHeight = (int)r.getHeight();
						CodeEditor.this.repaint( 0, (int)r.getY(), CodeEditor.this.getWidth(), (int)r.getHeight() );
					}
					else
						CodeEditor.this.currentLineHeight = -1;
				}
				catch( final BadLocationException err ) {
					Err.show( err );
				}
		} );


		// --- Auto-Indent ---
		this.addKeyListener( new KeyListener() {

			public void keyPressed( final KeyEvent e ) {}

			public void keyReleased( final KeyEvent e ) {

				if( e.getKeyCode() == 10 && e.getModifiersEx() == 0 ) {
					final String text = CodeEditor.this.getText();
					final int base = CodeEditor.this.getCaretPosition();
					int startBefore = base - 2;

					if( text.length() == 0 )
						return;

					while( startBefore > 0 && text.charAt( startBefore ) != '\n' )
						startBefore--;
					final String lineBefore = text.substring( startBefore + 1, base - 1 );
					final int indent = Lib_Parser.getDepth( lineBefore );
					if( indent > 0 )
						try {
							CodeEditor.this.getDocument().insertString( base, Lib_String.sequence( '\t', indent ), null );
						}
						catch( final BadLocationException err ) {
							Err.show( err );
						}
				}
			}

			public void keyTyped( final KeyEvent e ) {}

		} );
	}

	public void find( final String find ) {
		if( this.style != null )
			this.style.paintFind( find );
	}

	public Fn_UndoRedo getHistory() {
		return this.history;
	}

	public int getLineCount() {
		return this.getDocument().getDefaultRootElement().getElementCount();
	}

	@Override
	public Dimension getPreferredSize() {
		// Avoid substituting the minimum width for the preferred width when the viewport is too narrow
		return this.getUI().getPreferredSize( this );
	}

	// --- No wrap lines ---
	@Override
	public boolean getScrollableTracksViewportWidth() {
		// Only track viewport width when the viewport is wider than the preferred width
		return this.getUI().getPreferredSize( this ).width <= this.getParent().getSize().width;
	}

	public void occurrences( final ArrayList<Section> mark ) {
		if( this.style != null )
			if( mark != null )
				this.style.paintOccurrences( mark );
			else
				this.style.repaint();
	}

	public void setCode( final String code ) {
		this.setText( code );
	}

	public void setStyler( final I_Styler style ) {
		this.style = style;
	}

	@Override
	protected void paintComponent( final Graphics g ) {

		// Highlight current line
		if( this.currentLineHeight > 0 ) {
			g.setColor( CodeEditor.LINE_HIGHLIGHT_COLOR );
			g.fillRect( 0, this.caretYcurrent, this.getWidth(), this.currentLineHeight );
		}

		// Get Font-Width		Dialog-Input:15 == 9
//		MOut.temp(g.getFontMetrics().charWidth('c'));

//		Graphics2D graphics2d = (Graphics2D) g;
//		graphics2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
//		graphics2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
//      graphics2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
//      graphics2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);

		super.paintComponent( g ); // Muss nach der "Highlight current line"! If opaque, draws white over the current line

		if( this.style != null )
			this.style.paintStyles();
	}

}
