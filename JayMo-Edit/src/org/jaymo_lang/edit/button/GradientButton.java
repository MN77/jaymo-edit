/*******************************************************************************
 * Copyright (C): 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit.button;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;

import javax.swing.JButton;


/**
 * @author Michael Nitsche
 * @created 28.10.2020
 */
public class GradientButton extends JButton {

	private static final long serialVersionUID = 1L;


	public GradientButton( final String text, final Color color ) {
		super( "Gradient Button" );

		this.setContentAreaFilled( false );
		this.setFocusPainted( false ); // TODO: Only for demonstration?
		this.setText( text );
		this.setBackground( color );
	}

	@Override
	protected void paintComponent( final Graphics g ) {
		final Graphics2D g2 = (Graphics2D)g.create();
		g2.setPaint( new GradientPaint(
			new Point( 0, 0 ),
			this.getBackground().brighter(),
			new Point( 0, this.getHeight() / 3 ),
			Color.WHITE ) );
		g2.fillRect( 0, 0, this.getWidth(), this.getHeight() / 3 );
		g2.setPaint( new GradientPaint(
			new Point( 0, this.getHeight() / 3 ),
			Color.WHITE,
			new Point( 0, this.getHeight() ),
			this.getBackground() ) );
		g2.fillRect( 0, this.getHeight() / 3, this.getWidth(), this.getHeight() );
		g2.dispose();

		super.paintComponent( g );
	}

}
