/*******************************************************************************
 * Copyright (C): 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit.action;

import org.jaymo_lang.edit.Main_JayMo_Edit;
import org.jaymo_lang.edit.defs.EditorMainPanel;
import org.jaymo_lang.edit.defs.TextOutput;
import org.jaymo_lang.edit.defs.TextResult;
import org.jaymo_lang.error.ErrorBaseDebug;
import org.jaymo_lang.lib.swing.control.scroll.composit.canvas.deco.JMo_Swing_Main;
import org.jaymo_lang.model.App;
import org.jaymo_lang.parser.Parser_App;

import de.mn77.base.debug.Stopwatch;
import de.mn77.base.error.Err;
import de.mn77.base.event.Procedure;
import de.mn77.base.sys.Sys;
import de.mn77.base.thread.A_ParallelProcess;


/**
 * @author Michael Nitsche
 * @created 25.04.2021
 */
public class ExecutionControl {

	private static final String TEXT_END_ERROR = "Process ended with error.";
	private static final String TEXT_END_EMPTY = "Nothing to do!";

	private final Parser_App parser;
	private App              runningApp    = null;
	private Thread           runningThread = null;


	public ExecutionControl() {
		this.parser = new Parser_App();
	}

	public void execute( final String sourceCode, final Main_JayMo_Edit frame, final EditorMainPanel compMain, final Procedure onFinish ) {
		if( this.runningThread != null )
			return;

		if( sourceCode.trim().length() == 0 ) {
			onFinish.execute(); // Reset buttons
			frame.setStatus( ExecutionControl.TEXT_END_EMPTY );
			return;
		}

		final TextOutput compOutput = compMain.getComponentOutput();
		final TextResult compResult = compMain.getComponentResult();
		frame.setStatus( "Running ..." );

		this.runningThread = new A_ParallelProcess() {

			@Override
			protected void onFinished() {
				onFinish.execute();
				ExecutionControl.this.runningApp = null;
				ExecutionControl.this.runningThread = null;
			}

			@Override
			protected void process() {
				if( ExecutionControl.this.runningApp != null )
					return;

				App app = null;

				// Empty output
				compOutput.setText( "" );
				compResult.showMessage( "" );
				compMain.showOutput();

				try {
//					final App app = parser.parseText(">strictSave");
					app = ExecutionControl.this.parser.parseText( ">strictWebstart" );
					ExecutionControl.this.parser.parseText( app, sourceCode );

//					final App app = parser.parseText(sourcecode);

					ExecutionControl.this.runningApp = app;

					// Write output direct
					app.setOutputRedirection( ( final String s ) -> {

						synchronized( this ) {
							compOutput.append( s );
							compOutput.setCaretPosition( compOutput.getText().length() ); // Scroll to the end of the text
						}
						compOutput.repaint();
					} );

//					app.describe();
					app.setNoHardExit();
					final Stopwatch timer = new Stopwatch();
					final String result = app.exec( null ); // 		<--------- Execute

					if( result != null ) {
						compResult.showMessage( result );
						frame.setStatus( "Runtime: " + ExecutionControl.this.iTimeMMSSsss( timer.getMillisec() ) );
					}
					else {
						compResult.showMessage( "Error" );
						frame.setStatus( ExecutionControl.TEXT_END_ERROR );
					}
				}
				catch( final ErrorBaseDebug ce ) {

					if( app == null || !app.toBeTerminated() ) {
						compOutput.setText( ce.toInfo() );
						compResult.showMessage( "Error" );
						frame.setStatus( ExecutionControl.TEXT_END_ERROR );
						Err.show( ce );
					}
				}
				catch( final Throwable t ) {
					compOutput.setText( t.toString() );
					compResult.showMessage( "Error" );
					frame.setStatus( ExecutionControl.TEXT_END_ERROR );
					Err.show( t );
				}
			}

		};
	}

	@SuppressWarnings( "deprecation" )
	public void reset() {
		JMo_Swing_Main.terminateAll();

		if( this.runningApp != null )
			//			MOut.print("Terminate");
			this.runningApp.terminate(); // Don't execute any further call

		Sys.sleep( 100 );

		if( this.runningThread != null )
			//			MOut.print("Interrupt", this.runningThread);
			this.runningThread.interrupt(); // Interrupt thread

		Sys.sleep( 100 );

		try {
			if( this.runningThread != null )
				//				MOut.print("Stop", this.runningThread);
				this.runningThread.stop(); // Hard end of thread
		}
		catch( final ThreadDeath td ) {}
	}

	private String iTimeMMSSsss( long time ) {
		final int ms = (int)(time % 1000);
		time = time / 1000;
		final int ss = (int)time;
		return ss == 0 ? "" + ms + " ms" : ss + "." + ms + " Sec.";
	}

}
