/*******************************************************************************
 * Copyright (C): 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit.action;

/**
 * @author Michael Nitsche
 * @created 23.03.2021
 */
public enum EVENTS {

	OPEN,
	RECENT,
	SAVE,
	SAVE_AS,
	LOAD,
	NEW,
	CUT,
	COPY,
	PASTE,
	SELECT_ALL,
	SELECT_NONE,
	EXEC,
	STOP,
	UNDO,
	REDO,
	INDENT,
	OUTDENT,
	INFO,
	WEB,
	EXIT,
	REFACTORING,
	FIND,
	CHANGED,
	SLIDER

}
