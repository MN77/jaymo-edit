/*******************************************************************************
 * Copyright (C): 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit.action;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.net.URI;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JSplitPane;
import javax.swing.filechooser.FileFilter;

import org.jaymo_lang.JayMo;
import org.jaymo_lang.edit.Main_JayMo_Edit;
import org.jaymo_lang.edit.defs.EditorMainPanel;
import org.jaymo_lang.edit.defs.FileEditor;
import org.jaymo_lang.edit.dialog.Dialog_About;
import org.jaymo_lang.edit.editor.funcs.Lib_IndentOutdent;
import org.jaymo_lang.edit.refactor.JayMo_Refactor;

import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.event.Procedure;
import de.mn77.base.sys.cmd.SysDefault;
import de.mn77.base.sys.file.Lib_TextFile;
import de.mn77.base.sys.file.MFile;
import de.mn77.lib.swing.DIALOG_TYPE;
import de.mn77.lib.swing.Lib_SwingDialog;


/**
 * @author Michael Nitsche
 * @created 23.03.2021
 */
public class JayMo_Actions implements ActionListener {

	private final Component       base;
	private final Main_JayMo_Edit frame;

	private EditorMainPanel        main   = null;
	private FileEditor             editor = null;
	private JButton                execButton;
	private JButton                terminateButton;
	private final ExecutionControl execControl;


	private final FileFilter jmoFilter = new FileFilter() {

		@Override
		public boolean accept( final File f ) {
			if( f.isDirectory() )
				return true;

			final MFile mf = new MFile( f );
			return mf.getSuffix().toLowerCase().equals( "jmo" );
		}

		@Override
		public String getDescription() {
			return "JayMo-Files (*.jmo)";
		}

	};


	public JayMo_Actions( final Main_JayMo_Edit frame, final Component base ) { //, EditorMainPanel main
		this.frame = frame;
		this.base = base;
		this.execControl = new ExecutionControl();
	}

	public void actionPerformed( final ActionEvent e ) {
		final String action = e.getActionCommand();

		if( action.equals( EVENTS.CHANGED.name() ) )
			this.frame.updateTitle( this.editor );

		else if( action.equals( EVENTS.EXEC.name() ) ) {
			this.main.clearOutput();
			this.execute();
		}

		else if( action.equals( EVENTS.STOP.name() ) )
			//			this.reset.execute();
//			this.execButton.setEnabled(true);
			this.terminate();
		else if( action.equals( EVENTS.NEW.name() ) )
			this.iNew();
		else if( action.equals( EVENTS.CUT.name() ) ) {
			this.editor.cut();
			this.editor.setChanged( true );
			this.editor.getHistory().add();
		}
		else if( action.equals( EVENTS.COPY.name() ) )
			this.editor.copy();
		else if( action.equals( EVENTS.PASTE.name() ) ) {
			this.editor.paste();
			this.editor.setChanged( true );
			this.editor.getHistory().add();
		}

		else if( action.equals( EVENTS.SELECT_ALL.name() ) )
			this.editor.selectAll();

		else if( action.equals( EVENTS.SELECT_NONE.name() ) ) {
			final int caretPos = this.editor.getCaretPosition();
			this.editor.select( caretPos, caretPos );
		}

		else if( action.equals( EVENTS.INDENT.name() ) ) {
			final boolean b = Lib_IndentOutdent.indent( this.editor, true );
			if( b )
				this.editor.getHistory().add();
			this.editor.setChanged( true );
		}
		else if( action.equals( EVENTS.OUTDENT.name() ) ) {
			final boolean b = Lib_IndentOutdent.outdent( this.editor );
			if( b )
				this.editor.getHistory().add();
			this.editor.setChanged( true );
		}

		else if( action.equals( EVENTS.OPEN.name() ) )
			this.iOpenDialog();
		else if( action.equals( EVENTS.SAVE.name() ) )
			this.iSave();
		else if( action.equals( EVENTS.SAVE_AS.name() ) )
			this.iSaveAsDialog();
		else if( action.equals( EVENTS.RECENT.name() ) )
			this.editor.getRecent().show( (JComponent)e.getSource() );
		else if( action.equals( EVENTS.UNDO.name() ) ) {
			this.editor.getHistory().undo();
			this.editor.setChanged( true );
		}
		else if( action.equals( EVENTS.REDO.name() ) ) {
			this.editor.getHistory().redo();
			this.editor.setChanged( true );
		}

		else if( action.equals( EVENTS.INFO.name() ) )
			Dialog_About.show( this.base );
		else if( action.equals( EVENTS.WEB.name() ) )
			try {
				SysDefault.openBrowser( new URI( JayMo.WEB_HOME_FULL ) );
			}
			catch( final Exception e1 ) {
				Err.show( e1 );
			}
		else if( action.equals( EVENTS.EXIT.name() ) ) {
			if( this.editor.hasUnsavedChanges() )
				Lib_SwingDialog.yesOrNo( this.base, DIALOG_TYPE.WARNING, "Script changed", "Save your work?", ( b ) -> {
					if( b )
						this.iSave();
					this.frame.dispose();
				} );
			else
				this.frame.dispose();
		}

		else if( action.startsWith( EVENTS.LOAD.name() ) ) {
			final String filename = action.substring( EVENTS.LOAD.name().length() + 1 );
			final File f = new File( filename );
			this.iOpen( f );
		}

		else if( action.startsWith( EVENTS.REFACTORING.name() ) ) {
			final int caretPosition = this.editor.getCaretPosition();
			final String text = JayMo_Refactor.rename( this.base, this.editor.getText(), caretPosition );

			if( text != null ) {
				this.editor.setText( text );
				this.editor.setCaretPosition( caretPosition );
				this.editor.getHistory().add();
				this.editor.setChanged( true );
			}
		}
		else if( action.startsWith( EVENTS.FIND.name() ) ) {
			final String find = Lib_SwingDialog.input( this.base, "Find", "Search for", null );
			if( find != null )
				this.editor.find( find );
		}
		else if( action.startsWith( EVENTS.SLIDER.name() ) ) {
			final JSplitPane split = this.main.getSplit();
			final int min = split.getMinimumDividerLocation();
			final int cur = split.getDividerLocation();
			split.setDividerLocation( cur <= min + 50 ? Integer.MAX_VALUE : min );
		}
	}

	public void open( final File initFile ) {
		this.iOpen( initFile );
	}

	public void perform( final EVENTS event ) {
//		this.actions.actionPerformed(new ActionEvent(event.getSource(), event.getID(), EVENTS.CUT.name()))
		this.actionPerformed( new ActionEvent( this, -1, event.name() ) );
	}

	public void setButtons( final JButton execButton, final JButton terminateButton ) {
		this.execButton = execButton;
		this.terminateButton = terminateButton;
	}

	public void setMain( final EditorMainPanel main ) {
		this.main = main;
		this.editor = main.getComponentEditor();

		this.editor.addKeyListener( new KeyListener() {

			public void keyPressed( final KeyEvent e ) {}

			public void keyReleased( final KeyEvent e ) {
				if( e.getKeyCode() == 'R' && e.getModifiersEx() == InputEvent.CTRL_DOWN_MASK )
					JayMo_Actions.this.perform( EVENTS.REFACTORING );
			}

			public void keyTyped( final KeyEvent e ) {}

		} );
	}

	private void execute() {
		if( !this.execButton.isEnabled() )
			return;

		this.execButton.setEnabled( false );
		this.terminateButton.setEnabled( true );

		final Procedure onFinish = () -> {
			this.execButton.setEnabled( true );
			this.terminateButton.setEnabled( false );
		};

		this.execControl.execute( this.main.getCode(), this.frame, this.main, onFinish );
	}

	private void iNew() {
		if( this.editor.hasUnsavedChanges() )
			Lib_SwingDialog.yesOrNo( this.base, DIALOG_TYPE.WARNING, "Script changed", "Save your work?", ( b ) -> {

				if( b ) {
					this.iSave();
					this.iNew(); // Try again
				}
			} );

		this.main.clearOutput();
		this.editor.setCode( "" );
		this.editor.setCurrentFile( null );
		this.editor.setChanged( false );
		this.editor.getHistory().clear();
		this.frame.setStatus( "New script" );
	}

	private void iOpen( final File f ) {
		if( this.editor.hasUnsavedChanges() )
			Lib_SwingDialog.yesOrNo( this.base, DIALOG_TYPE.WARNING, "Script changed", "Save your work?", ( b ) -> {

				if( b ) {
					this.iSave();
					this.iOpen( f ); // Try again
				}
			} );

		try {
			final boolean createNew = !f.exists();

			final String code = createNew
				? ""
				: Lib_TextFile.read( f );
			this.editor.setCode( code );

			this.editor.setCurrentFile( f );
			this.editor.setChanged( false );
			this.editor.getRecent().add( f );
			this.main.getComponentOutput().setText( "" );
			this.main.getComponentResult().setText( "" );

			final String taskMessage = createNew
				? "New File: "
				: "File opened: ";
			this.frame.setStatus( taskMessage + f.getAbsolutePath() );

			this.editor.getHistory().clear();
			this.editor.getHistory().add();
		}
		catch( final Err_FileSys e ) {
			Err.show( e );
			Lib_SwingDialog.okay( this.base, DIALOG_TYPE.ERROR, "File open error", e.getMessage() );
		}
	}

	private void iOpenDialog() {
		final File current = this.editor.getCurrentFile();
		final String startDir = current == null ? null : current.getAbsolutePath();

		Lib_SwingDialog.open( this.base, startDir, this.jmoFilter, f -> {
			if( new MFile( f ).getSuffix().toLowerCase().equals( "jmo" ) )
				if( !f.exists() )
					Lib_SwingDialog.okay( this.base, DIALOG_TYPE.ERROR, "File read error", "File doesn't exist: " + f.getAbsolutePath() );
				else
					this.iOpen( f );
			else
				Lib_SwingDialog.okay( this.base, DIALOG_TYPE.ERROR, "Invalid file type", "The selected file is not a JayMo-File." );
		} );
	}

	private void iSave() {
		final File current = this.editor.getCurrentFile();
		if( current == null )
			this.iSaveAsDialog();
		else
			this.iSave( current );
	}

	private void iSave( final File f ) {
		final String code = this.editor.getText();

		try {
			Lib_TextFile.set( f, code );
			this.editor.setCurrentFile( f );
			this.editor.setChanged( false );
			this.editor.getRecent().add( f );
			this.frame.setStatus( "File saved: " + f.getAbsolutePath() );
		}
		catch( final Err_FileSys e ) {
			Err.show( e );
		}
	}

	private void iSaveAsDialog() {
		final File current = this.editor.getCurrentFile();
		final String startDir = current == null ? null : current.getAbsolutePath();
		Lib_SwingDialog.save( this.base, startDir, this.jmoFilter, f -> {
			final MFile mf = new MFile( f );
			if( !mf.getSuffix().toLowerCase().equals( "jmo" ) )
				mf.setSuffix( "jmo" );
			if( mf.exists() )
				Lib_SwingDialog.yesOrNo( this.base, DIALOG_TYPE.WARNING, "File already exists", "Really overwrite '" + mf.getPathAbsolute() + "'?", b -> {
					if( b != null && b )
						this.iSave( mf.getFile() );
				} );
			else
				this.iSave( mf.getFile() );
		} );
	}

	private void terminate() {
		this.execControl.reset();
		this.execButton.setEnabled( true );
		this.frame.setStatus( "Terminated" );
	}

}
