/*******************************************************************************
 * Copyright (C): 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit.dialog;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.io.InputStream;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

import org.jaymo_lang.JayMo;
import org.jaymo_lang.edit.Main_JayMo_Edit;

import de.mn77.base.data.util.Lib_GoldenRatio;
import de.mn77.base.data.util.Lib_String;
import de.mn77.base.error.Err;
import de.mn77.base.stream.Lib_Stream;
import de.mn77.base.sys.file.Lib_Jar;


/**
 * @author Michael Nitsche
 * @created 12.04.2021
 */
public class Dialog_License {

	public static void main( final String[] args ) { // TEST
		final JFrame frame = new JFrame();
		Dialog_License.show( frame );
		frame.dispose();
	}

	public static void show( final Component parent ) {
		Err.ifNull( parent );

		final StringBuilder sb = new StringBuilder();
		sb.append( Main_JayMo_Edit.NAME + '\n' );
		sb.append( "--------------------------------------\n" );
		sb.append( "Copyright © 2017-2025 Michael Nitsche <code@mn77.de>\n" );
		sb.append( "\n" );
		sb.append( "This program comes with ABSOLUTELY NO WARRANTY!\n" );
		sb.append( "This is free software, and you are welcome to redistribute it under certain conditions!\n" );
		sb.append( "\n" );

		sb.append( "Although this programm is free software the author asks for support (e.g. via Patreon)\nfor the countless hours of hard work.\n" );
		sb.append( "Thank you very much.\n" );

		sb.append( "\n" );
		sb.append( "For contact and additional informations please visit: https://www.jmo-lang.org\n" );

		Dialog_License.addLine( sb );

		sb.append( "Components in this compilation:\n" );
		sb.append( "\n" );
		sb.append( "  • " + Main_JayMo_Edit.NAME + ":   GPL v3\n" );
		sb.append( "  • " + JayMo.NAME + "-Core:   LGPL v3\n" );
		sb.append( "  • " + JayMo.NAME + "-Lib:   LGPL v3\n" );
		sb.append( "  • " + JayMo.NAME + "-Ext:   LGPL v3\n" );
		sb.append( "  • MN77-Libs:   LGPL v3\n" );
		sb.append( "  • Font \"Liberation2\":   OFL\n" );
		sb.append( "  • Font \"DejaVu Sans Mono\":   Proprietary\n" );
//		sb.append("  • Font \"C64 Pro Mono STYLE\":   Proprietary\n");
		sb.append( "  • Tango Icons:   Public Domain\n" );
		Dialog_License.addLine( sb );

		try {
			InputStream is = Lib_Jar.getStream( "/jar/license/GPL_V3.TXT" );
			sb.append( Lib_Stream.readUTF8( is ) );
			Dialog_License.addLine( sb );

			is = Lib_Jar.getStream( "/jar/license/LGPL_V3.TXT" );
			sb.append( Lib_Stream.readUTF8( is ) );
			Dialog_License.addLine( sb );
//
			is = Lib_Jar.getStream( "/jar/icon/COPYING.TXT" );
			sb.append( Lib_Stream.readUTF8( is ) );
			Dialog_License.addLine( sb );

//			is = Lib_Jar.getJarStream("/jar/font/c64/COPYING.TXT");
//			sb.append( Lib_Stream.readUTF8(is) );
//			Dialog_License.addLine(sb);

			is = Lib_Jar.getStream( "/jar/font/dejavu/COPYING.TXT" );
			sb.append( Lib_Stream.readUTF8( is ) );
//			Dialog_License.addLine(sb);
		}
		catch( final Exception e ) {
			Err.exit( e );
		}


		final JTextPane jtext = new JTextPane();
//		StyledDocument doc = jtext.getStyledDocument();
//		jtext.setDocument(doc);
//		jtext.setAutoscrolls(true);
//		jtext.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1, false));
		jtext.setEditable( false );
		jtext.setMargin( new Insets( 5, 5, 5, 5 ) );

//		SimpleAttributeSet attrs=new SimpleAttributeSet();
//      StyleConstants.setAlignment(attrs,StyleConstants.ALIGN_CENTER);
//      doc.setParagraphAttributes(0,doc.getLength()-1,attrs,false);

		jtext.setText( sb.toString() );
		jtext.setCaretPosition( 0 );
		jtext.setOpaque( true );
		final int dialogWidth = 700;
		jtext.setPreferredSize( new Dimension( dialogWidth, Lib_GoldenRatio.longSideAsInt( dialogWidth ) ) );

		final JScrollPane scroll1 = new JScrollPane( jtext );

		final JComponent[] message = {
			scroll1
		};

		JOptionPane.showConfirmDialog( parent, message, Main_JayMo_Edit.NAME + " - License", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE );
	}

	private static void addLine( final StringBuilder sb ) {
		sb.append( "\n" + Lib_String.sequence( '—', 50 ) + "\n\n" );
	}

}
