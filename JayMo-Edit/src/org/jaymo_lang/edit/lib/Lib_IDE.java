/*******************************************************************************
 * Copyright (C): 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit.lib;

/**
 * @author Michael Nitsche
 * @created 29.03.2021
 */
public class Lib_IDE {

	public static final String BRACKETS   = "(){}<>[]";
	public static final String SHORTCUTS  = "°§"; // &± %$€
	public static final String SHORTCUTS2 = "°"; // $ (°° $$)


	public static boolean isAtomicWord( final String word ) {

		switch( word ) { //.toLowerCase()
			case "false":
//			case "False":
			case "FALSE":

			case "nil":
//			case "Nil":
			case "NIL":

			case "null":
//			case "Null":
			case "NULL":

			case "true":
//			case "True":
			case "TRUE":

			case "infinity":
			case "not_a_number":
				return true;
		}
		return false;
	}

	public static boolean isMagicVariable( final String word ) {

		switch( word ) { //.toLowerCase()
			case "app":
			case "cur":
			case "each":

			case "func":
			case "it":
			case "jmo":
			case "jaymo":

			case "loop":
			case "this":
			case "super":

//			case "event":
				return true;
		}
		return false;
	}

}
