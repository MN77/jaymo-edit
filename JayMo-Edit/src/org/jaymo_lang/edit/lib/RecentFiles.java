/*******************************************************************************
 * Copyright (C): 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit.lib;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.jaymo_lang.edit.action.EVENTS;
import org.jaymo_lang.edit.action.JayMo_Actions;

import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.file.I_Directory;
import de.mn77.base.sys.file.SysDir;
import de.mn77.lib.config.PropertiesFile;


/**
 * @author Michael Nitsche
 * @created 27.03.2021
 */
public class RecentFiles {

	private static final int MAX_ITEMS = 20;

	private PropertiesFile  prop    = null;
	private JayMo_Actions   actions = null;
	private ArrayList<File> files   = null;


	public void add( final File f ) {
		if( this.files.contains( f ) )
			this.files.remove( f ); // Move it to the end = first item in menu
		this.files.add( f );

		while( this.files.size() > RecentFiles.MAX_ITEMS )
			this.files.remove( 0 ); // Remove first

		try {
			final int amount = this.files.size();
			this.prop.setInteger( "files", amount );
			for( int i = this.files.size(); i > 0; i-- ) // Last added comes at first
				this.prop.setString( "file" + i, this.files.get( i - 1 ).getAbsolutePath() );
			this.prop.write();
		}
		catch( final Err_FileSys e ) {
			Err.show( e );
		}
	}

	public void init( final JayMo_Actions actions ) {
		this.actions = actions;

		try {
			final I_Directory dir = SysDir.userCache_MN77( "JayMo" );
			this.prop = new PropertiesFile( "", dir.fileMay( "recent", "list" ).getFile() );
			this.prop.read( false );
			final int amount = this.prop.getIntegerOrDefault( "files", 0 );
			this.files = new ArrayList<>( amount );

			for( int i = 1; i <= amount; i++ ) {
				final String s = this.prop.getStringOrDefault( "file" + i, null );

				if( s != null ) {
					final File f = new File( s );
					if( f.exists() )
						this.files.add( f );
//					else
//						MOut.temp(f);
				}
			}
//			MOut.temp(this.files);
		}
		catch( final Err_FileSys e ) {
			Err.show( e );
		}
	}

	public void show( final JComponent comp ) {
		final JPopupMenu menu = new JPopupMenu();

		for( int i = this.files.size() - 1; i >= 0; i-- ) { // Last added comes at first
			final File f = this.files.get( i );
			String name = f.getName();
			if( name.endsWith( ".jmo" ) )
				name = name.substring( 0, name.length() - 4 );
			final JMenuItem item = new JMenuItem( name );
			item.addActionListener( e -> {
				final ActionEvent event2 = new ActionEvent( e.getSource(), e.getID(), EVENTS.LOAD.name() + " " + f.getAbsolutePath() );
				this.actions.actionPerformed( event2 );
			} );
			menu.add( item );
		}

		menu.show( comp, 0, comp.getHeight() );
	}

}
